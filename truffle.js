module.exports = {
  networks: {
    development: {
      host: "localhost",
      port: 8545,
      gasPrice: 10000000000, // deploy with a gas price of 10 gwei
      network_id: "5" // TestRPC network id
    },
    staging: {
      host: "localhost",
      port: 8545,
      gasPrice: 10000000000, // deploy with a gas price of 10 gwei
      network_id: "4" // Rinkeby network id
    },
    production: {
      host: "localhost",
      port: 8545,
      gasPrice: 10000000000, // deploy with a gas price of 10 gwei
      network_id: "1" // Public  network id
    }
  }
};
