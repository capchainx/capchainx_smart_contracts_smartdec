// Abstract contract implementing the ERC20 token standard:
// See https://github.com/ethereum/EIPs/blob/master/EIPS/eip-20-token-standard.md

pragma solidity ^0.4.8;

contract AbstractToken {

    // the name of the token
    string name;

    // the symbol of the token
    string symbol;

    // the number of decimals the token uses (divide balance by this number to get human-readable balance)
    uint8 decimals;

    // the total supply of the token
    uint256 totalSupply;

    // returns the balance of the account _owner
    function balanceOf(address _owner) constant returns (uint256 balance);

    // transfers _value tokens from the sender to _to
    function transfer(address _to, uint256 _value) returns (bool success);

    // transfers _value tokens from _from to _to (requires prior approval via approve function)
    function transferFrom(address _from, address _to, uint256 _value) returns (bool success);

    // allows _spender to withdraw up to _value tokens from the sender's balance
    function approve(address _spender, uint256 _value) returns (bool success);

    // returns the amount that _spender is allowed to withdraw on _owner's behalf
    function allowance(address _owner, address _spender) constant returns (uint256 remaining);

    // must be triggered whenever transfer or transferFrom are called successfully
    event Transfer(address indexed _from, address indexed _to, uint256 _value);

    // must be triggered whenever approve is called successfully 
    event Approval(address indexed _owner, address indexed _spender, uint256 _value);
}
