// contract implementing the Cryptoequity token

pragma solidity ^0.4.8;

import "./AbstractToken.sol";

contract CryptoequityToken is AbstractToken {

    // state variables

    string public name;
    string public symbol;
    uint8 public decimals = 6;
    uint256 public totalSupply; 

    mapping (address => uint256) balances;
    mapping (address => mapping (address => uint256)) allowances;

    // functions

    function CryptoequityToken(string _name, string _symbol, uint _equityBasisPoints) {

        // set the values of the state variables
        name = _name;
        symbol = _symbol;
        totalSupply = _equityBasisPoints * 10000000000; // each equity percent is a million tokens

        // allocate the total supply to the creator of the contract
        balances[msg.sender] = totalSupply;

        // log a transfer for this initial allocation (as per the ERC20 standard)
        Transfer(0x0, msg.sender, totalSupply);
    }

    function balanceOf(address _owner) constant returns (uint256 balance) {
        return balances[_owner];
    }

    function transfer(address _to, uint256 _value) returns (bool success) {

        // check that the sender has enough tokens
        require(balances[msg.sender] >= _value);
        
        // check for overflow
        require(balances[_to] + _value >= balances[_to]);

        // check that the receiving address is not the zero address
        require(_to != address(0));

        // decrease the sender's balance by _value
        balances[msg.sender] -= _value;

        // increase the receiver's balance by _value
        balances[_to] += _value;

        // log a Transfer event
        Transfer(msg.sender, _to, _value);

        // return true to indicate a successful transfer
        return true;
    }

    function transferFrom(address _from, address _to, uint256 _value) returns (bool success) {
        
        // check that the giver has enough balance
        require(balances[_from] >= _value);

        // check that the sender is allowed to transfer on behalf of the giver
        require(allowances[_from][msg.sender] >= _value);

        // check for overflow
        require(balances[_to] + _value >= balances[_to]);

        // check that the receiving address is not the zero address
        require(_to != address(0));

        // decrease the giver's balance by _value
        balances[_from] -= _value;

        // increase the receiver's balance by _value
        balances[_to] += _value;

        // decrease the giver's allowance to the sender by _value
        allowances[_from][msg.sender] -= _value;

        // log a Transfer event
        Transfer(_from, _to, _value);

        // return true to indicate a successful transfer
        return true;
    }

    function approve(address _spender, uint256 _value) returns (bool success) {

        // set the sender's allowance to _spender to _value
        allowances[msg.sender][_spender] = _value;

        // log an Approval event
        Approval(msg.sender, _spender, _value);

        // return true to indicate a successful approval
        return true;
    }

    function allowance(address _owner, address _spender) constant returns (uint256 remaining) {

        // return _owner's allowance to _spender
        return allowances[_owner][_spender];
    }

    function increaseApproval(address _spender, uint _increase) public returns (bool) {

        // calculate the new allowance as the sum of the old allowance and the increase value
        uint newAllowance = allowances[msg.sender][_spender] + _increase;

        // set the sender's allowance to the new allowance
        allowances[msg.sender][_spender] = newAllowance;

        // log an Approval event
        Approval(msg.sender, _spender, newAllowance);

        // return true to indicate a successful approval
        return true;
    }

    function decreaseApproval(address _spender, uint _decrease) public returns (bool) {

        // initialize the new allowance as zero
        uint newAllowance = 0;
        
        // if the old allowance is greater than the decrease value, set the new allowance as the difference
        if (allowances[msg.sender][_spender] > _decrease) {
            newAllowance = allowances[msg.sender][_spender] - _decrease;
        }

        // set the sender's allowance to the new allowance
        allowances[msg.sender][_spender] = newAllowance;

        // log an Approval event
        Approval(msg.sender, _spender, newAllowance);

        // return true to indicate a successful approval
        return true;
    }
}
