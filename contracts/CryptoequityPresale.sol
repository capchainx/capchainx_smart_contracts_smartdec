// contract implementing a cryptoequity presale

pragma solidity ^0.4.8;

import "./CryptoequityToken.sol";

contract CryptoequityPresale {


    /*******************/
    /* State Variables */
    /*******************/
    
    // the cryptoequity ERC20 token contract
    CryptoequityToken public cryptoequityToken;

    // the address of the presale controller
    address public presaleController;

    // a bool indicating whether the killswitch has been thrown
    bool public isEmergency = false;

    // the number of presale rounds 
    uint8 public numRounds;

    // the allocations of the presale rounds
    uint[4] public roundAllocations;

    // the timestamps of the presale rounds
    uint[4] public roundTimestamps;

    // the discount percentages associated with each round 
    uint[4] public roundDiscountPercentages;

    // the base price of the token
    uint public basePrice;

    // the timestamp at which the presale ends
    uint public endTimestamp;


    /*************/
    /* Modifiers */
    /*************/

    // ensures that the caller is the presale controller
    modifier only_controller() {
        require(msg.sender == presaleController);
        _;
    }

    // ensures that the presale has started but not ended
    modifier while_going() {
        require(isStarted() && !isEnded());
        _;
    }

    // ensures that the presale has ended
    modifier after_ended() {
        require(isEnded());
        _;
    }


    /************************/
    /* Constructor Function */
    /************************/

    // creates a new Cryptoequity Presale contract
    function CryptoequityPresale(address _presaleController, address _cryptoequityToken, uint8 _numRounds, uint[4] _roundAllocations, uint[4] _roundTimestamps, uint[4] _roundDiscountPercentages, uint _basePrice, uint _endTimestamp) {

        // set values of state variables from constructor parameters
        presaleController = _presaleController;
        cryptoequityToken = CryptoequityToken(_cryptoequityToken);
        numRounds = _numRounds;
        roundAllocations = _roundAllocations;
        roundTimestamps = _roundTimestamps;
        roundDiscountPercentages = _roundDiscountPercentages;
        basePrice = _basePrice;
        endTimestamp = _endTimestamp;
    }


    /**********************/
    /* Internal Functions */
    /**********************/

    // returns the number of unsold tokens remaining in the current round for a given total number of tokens remaining
    function tokensRemainingInRound(uint _tokensRemaining) internal constant while_going returns (uint _tokensRemainingInRound) {

        // initialize the number of tokens remaining in the round
        _tokensRemainingInRound = _tokensRemaining;

        // iterate over the rounds
        for (uint i = numRounds - 1; i >= 1; i--) {

            // if the number of tokens remaining is greater than the round allocations...
            if (_tokensRemaining > roundAllocations[i]) {

                // set the tokens remaining in the round to be the difference
                _tokensRemainingInRound = _tokensRemaining - roundAllocations[i];
            }
        }

        // return the number of unsold tokens remaining in the current round
        return _tokensRemainingInRound;
    }

    // returns the price of a token at the current time for a given number total number of tokens remaining
    function tokenPrice(uint _tokensRemaining) internal constant while_going returns (uint _tokenPrice) {

        // if the auction has not started or has already ended, return a price of zero
        if (!isStarted() || isEnded()) {
            return 0;
        }
        
        // iterate over the rounds
        for (uint i = 0; i < numRounds; i++) {

            // if either the timestamp or the allocation for a round has passed, update the current price
            if (now > roundTimestamps[i] || _tokensRemaining <= roundAllocations[i]) {
                _tokenPrice = basePrice * roundDiscountPercentages[i] / 100;
            }
        }

        // return the token price
        return _tokenPrice;
    }


    /********************/
    /* Public Functions */
    /********************/

    // checks if the presale has started yet
    function isStarted() public constant returns (bool _isStarted) {

        // return true if the current timestamp is past the first round timestamp, otherwise return false
        return now >= roundTimestamps[0];
    }

    // checks if the presale has ended yet
    function isEnded() public constant returns (bool _isEnded) {

        // return true if an emergency has been declared, or if the current timestamp is past the end timestamp
        return isEmergency || now >= endTimestamp;
    }

    // returns the number of unsold tokens remaining
    function tokensRemaining() public constant while_going returns (uint256 _tokensRemaining) {

        // return the token balance of the presale contract (corrected for decimal places)
        return cryptoequityToken.balanceOf(this);
    }

    // returns the number of unsold tokens remaining in the current round
    function tokensRemainingInRound() public constant while_going returns (uint256 _tokensRemainingInRound) {

        // return the number of unsold tokens remaining in the current round for the current total number remaining
        return tokensRemainingInRound(tokensRemaining());
    }

    // returns the number of tokens that can be purchased for a given price
    function tokensForPrice(uint weiPrice) public constant while_going returns (uint _tokensForPrice) {

        // initialize the number of tokens remaining
        uint _tokensRemaining = tokensRemaining();

        // initialize the count of tokens available for the given price
        _tokensForPrice = 0;

        // loop until there are no tokens remaining
        while (_tokensRemaining > 0) {

            // get the price of a single token in the current round
            uint _tokenPrice = tokenPrice(_tokensRemaining);

            // exit if the price of a token is greater than the wei price
            if (_tokenPrice > weiPrice) {
                break;
            }

            // return zero if the token price is zero
            if (_tokenPrice == 0) {
                return 0;
            }

            // get the price of all tokens remaining in the round
            uint _tokensRemainingInRound = tokensRemainingInRound(_tokensRemaining);
            uint _roundPrice = _tokenPrice * _tokensRemainingInRound;

            // if the wei price is less than the round price, set the round price to the wei price
            if(_roundPrice >= weiPrice) {
               _roundPrice = weiPrice;
            }

            // get the number of tokens that can be purchased in the round
            uint _roundTokens = _roundPrice / _tokenPrice;

            // adjust the tokens for price, tokens remaining, and wei price
            _tokensForPrice += _roundTokens;
            _tokensRemaining -= _roundTokens;
            weiPrice -= _roundTokens * _tokenPrice;
        }
        
        // return the price for the tokens
        return _tokensForPrice;

    }

    // returns the current price for a given number of tokens
    function priceForTokens(uint numTokens) public constant while_going returns (uint _priceForTokens) {

        // get the current number of tokens remaining
        uint _tokensRemaining = tokensRemaining();

        // require that the number of tokens queried for be less than or equal to the current number of tokens remaining
        require(numTokens <= _tokensRemaining);

        // initialize the price for the tokens at zero
        _priceForTokens = 0;

        while (numTokens > 0) {
            uint _tokenPrice = tokenPrice(_tokensRemaining);
            uint _tokensRemainingInRound = tokensRemainingInRound(_tokensRemaining);
            if(_tokensRemainingInRound >= numTokens) {
                _tokensRemainingInRound = numTokens;
            }

            _priceForTokens += _tokenPrice * _tokensRemainingInRound;
            _tokensRemaining -= _tokensRemainingInRound;
            numTokens -= _tokensRemainingInRound;
        }
        
        // return the price for the tokens
        return _priceForTokens;
    }

    // fallback function allows the sender to buy tokens at the current price
    function() payable while_going {
        
        // get the amount of tokens that can be purchased for the supplied amount of wei
        uint _tokensForPrice = tokensForPrice(msg.value);

        // require that the number of tokens that can be purchased is greater than zero
        require(_tokensForPrice > 0);

        // get the price for the amount of tokens that can be purchased for the supplied amount of wei
        uint _priceForTokens = priceForTokens(_tokensForPrice);

        // get the amount by which the message sender overpaid for the tokens they will receive
        uint _overpaidWei = msg.value - _priceForTokens;

        // transfer purchased tokens to the purchaser, and require success
        require(cryptoequityToken.transfer(msg.sender, _tokensForPrice));

        // transfer overpaid wei back to the sender
        msg.sender.transfer(_overpaidWei);
    }


    /************************/
    /* Controller Functions */
    /************************/

    // called by the controller to withdraw the ether raised so far
    function withdrawEther() only_controller {

        // return all collected ether to the controller
        presaleController.transfer(this.balance);
    }

    // called by the controller to withdraw a certain number of tokens from the presale
    function withdrawTokens(uint numTokens) only_controller {

        // return the specified number of tokens to the controller
        require(cryptoequityToken.transfer(presaleController, numTokens));

    }

    // called by the controller once the auction has ended to withdraw the raised ether and unsold tokens
    function finalizePresale() only_controller after_ended {

        // return all collected ether to the controller
        presaleController.transfer(this.balance);

        // return all unsold tokens to the controller
        require(cryptoequityToken.transfer(presaleController, cryptoequityToken.balanceOf(this)));
    }

    // a killswitch to end the presale early and halt all buying operation
    function declareEmergency() only_controller {

        // set the killswitch bool to true
        isEmergency = true;
    }
}
