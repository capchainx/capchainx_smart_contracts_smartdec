// import the token contract
var CryptoequityToken = artifacts.require("./CryptoequityToken.sol");

module.exports = function(deployer, network, accounts) {

    // initialize the token params
    var tokenParams = {};
        
    if (network == 'development' || network == 'staging') {
        // import the test token parameters
        tokenParams = require("../params/testTokenParams.json");
    } else {
        // import the appropriate token parameters
        tokenParams = require("../params/ideasTokenParams.json"); // XXX CHANGE WHEN DEPLOYING
    }

    // deploy the token from account zero
    deployer.deploy(CryptoequityToken, tokenParams.name, tokenParams.symbol, tokenParams.equityBasisPoints, 
                    {from: accounts[0], gasPrice: 10000000000});
};
