var CryptoequityToken = artifacts.require("./CryptoequityToken.sol");
var CryptoequityPresale = artifacts.require("./CryptoequityPresale.sol");

module.exports = function(deployer, network, accounts) {

    deployer.then(function() {

        // load the deployed CryptoequityToken
        return CryptoequityToken.deployed();

    }).then(function(instance) {
        //instance = {address: null}; // XXX only needed for production deployment

        // XXX XXX XXX

        // set all customizable params here
        var customPresaleController = '0x01dd001e344c661B0C82b3FDDA3B97e70B318eED';
        var customCryptoequityToken = '0xbcf00d9af3e0785c2ff1954fdab1be6b58a6b12b';
        var customPresaleParams = require("../params/usatPresaleParams.json");

        // XXX XXX XXX


        // use account zero as the default controller address
        var presaleController = accounts[0];

        // use the address of previously deployed token as the default token address
        var cryptoequityToken = instance.address;

        // initialize the presale parameters
        var presaleParams = {};

        // if we're in development...
        if (network == 'development') {

            // load the presale params from the test params file
            presaleParams = require("../params/testPresaleParams.json")

            // in development, each round lasts 20 seconds, starting 20 seconds from the current time
            var nextRound = Math.floor((new Date).getTime()/1000);
            for (var i = 0; i < 5; i++) {
                nextRound += 20;
                presaleParams.roundTimestamps.push(nextRound);
            }
            presaleParams.endTimestamp = nextRound + 20;
        }

        // if we're in staging...
        else if (network == 'staging') {

            // load the presale params from the test params file
            presaleParams = require("../params/testPresaleParams.json")

            // in staging, each round lasts 5 minutes, starting 5 minutes from the current time
            var nextRound = Math.floor((new Date).getTime()/1000);
            for (var i = 0; i < 5; i++) {
                nextRound += 300;
                presaleParams.roundTimestamps.push(nextRound);
            }
            presaleParams.endTimestamp = nextRound + 300;
        }

        // if we're in production...
        else if (network == 'production') {

            // load the presale params from the appropriate params file
            presaleParams = customPresaleParams;

            // set the presale controller to the custom controller address if available
            if (customPresaleController != null) {
                presaleController = customPresaleController;
            }

            // set the token address to the custom token address if available
            if (customCryptoequityToken != null) {
                cryptoequityToken = customCryptoequityToken;
            }
        }

        // finally, deploy the presale contract
        return deployer.deploy(CryptoequityPresale, presaleController, cryptoequityToken, presaleParams.numRounds, 
                               presaleParams.roundAllocations, presaleParams.roundTimestamps, 
                               presaleParams.roundDiscountPercentages, presaleParams.basePrice, 
                               presaleParams.endTimestamp, {from: accounts[0], gasPrice: 10000000000});
    });
};
