var CryptoequityToken = artifacts.require("./CryptoequityToken.sol");
var CryptoequitySale = artifacts.require("./CryptoequitySale.sol");

module.exports = function(deployer, network, accounts) {

    deployer.then(function() {

        // load the deployed CryptoequityToken
        return CryptoequityToken.deployed();

    }).then(function(instance) {
        //instance = {address: null}; // XXX only needed for production deployment

        /* XXX XXX XXX */

        // set all customizable params here
        var customSaleController = '0x483439E9C03ceA88d0346d6AFe82bd50C7dBfA4C';
        var customCryptoequityToken = '0x2c665fd2678809aef8299688009ce01a56d38408';
        var customSaleParams = require("../params/kropsSaleParams.json");

        /* XXX XXX XXX */


        // use account zero as the default controller address
        var saleController = accounts[0];

        // use the address of previously deployed token as the default token address
        var cryptoequityToken = instance.address;

        // initialize the sale parameters
        var saleParams = {};

        // if we're in development...
        if (network == 'development') {

            // load the sale params from the test params file
            saleParams = require("../params/testSaleParams.json")

            // in development, the presale starts after 10 seconds and lasts 60 seconds
            var currentTime = Math.floor((new Date).getTime()/1000);
            saleParams.startTimestamp = currentTime + 10;
            saleParams.endTimestamp = currentTime + 70;
        }

        // if we're in staging...
        else if (network == 'staging') {

            // load the sale params from the test params file
            saleParams = require("../params/testSaleParams.json")

            // in staging, the presale starts after 5 minutes and lasts 10 minutes
            var currentTime = Math.floor((new Date).getTime()/1000);
            saleParams.startTimestamp = currentTime + 300;
            saleParams.endTimestamp = currentTime + 900;
        }

        // if we're in production...
        else if (network == 'production') {

            // load the sale params from the appropriate params file
            saleParams = customSaleParams;

            // set the sale controller to the custom controller address if available
            if (customSaleController != null) {
                saleController = customSaleController;
            }

            // set the token address to the custom token address if available
            if (customCryptoequityToken != null) {
                cryptoequityToken = customCryptoequityToken;
            }
        }

        // finally, deploy the sale contract
        return deployer.deploy(CryptoequitySale, saleController, cryptoequityToken, saleParams.numRounds, 
                               saleParams.roundAllocations, saleParams.startTimestamp, 
                               saleParams.roundMarkupPercentages, saleParams.basePrice, 
                               saleParams.endTimestamp, {from: accounts[0], gasPrice: 10000000000});
    });
};
