var Web3 = require('web3');
var BN = require('bignumber.js');
var sleep = require('sleep');

var CryptoequityPresale = artifacts.require("./CryptoequityPresale.sol");
var CryptoequityToken = artifacts.require("./CryptoequityToken.sol");



contract('CryptoequityPresaleOverpay', function(accounts) {

    /*************/
    /* Utilities */
    /*************/

    // before each test, initialize the cryptoequityToken and cryptoequityPresale variables from the deployed contracts
    beforeEach(async () => {
	cryptoequityPresale = await CryptoequityPresale.deployed();
        cryptoequityToken = await CryptoequityToken.deployed();
    })

    const promisify = (inner) =>
      new Promise((resolve, reject) =>
        inner((err, res) => {
          if (err) { reject(err) }
          resolve(res);
        })
      );
    
    const sendTransaction = (data) =>
      promisify(cb => web3.eth.sendTransaction(data, cb));


    const getBalance = (address) =>
      promisify(cb => web3.eth.getBalance(address, cb));

    //const gasPrice = 10000000000;
    const gasPrice = 1;
    const gas = 150000;


    /************************/
    /* Emergency User Tests */
    /************************/
    it("send initial tokens and wait until first round starts", async () => {
        console.log('Waiting for first round...');
        const firstRoundStart = await cryptoequityPresale.roundTimestamps.call(0)
        while (Math.floor(Date.now()/1000) <= firstRoundStart.valueOf()) {};
        console.log('First round started.');

        cryptoequityToken.transfer(cryptoequityPresale.address, 90000000000000, {from: accounts[0], gasPrice: gasPrice});

        const isStarted = await cryptoequityPresale.isStarted.call();
    })

    it("overpaying for one token refunds extra ether", async () => {

        // get the initial ether balances of the buyer and the presale
        const buyerInitialEtherBalance = await getBalance(accounts[1]);
        const presaleInitialEtherBalance = await getBalance(cryptoequityPresale.address);

        // get the initial token balances of the buyer and the presale
        const buyerInitialTokenBalance = await cryptoequityToken.balanceOf(accounts[1]);
        const presaleInitialTokenBalance = await cryptoequityToken.balanceOf(cryptoequityPresale.address);

        // buy one token
	await sendTransaction({to: cryptoequityPresale.address, from: accounts[1], value: 99, gasPrice: gasPrice, gas: gas});

        // get the final ether balances of the buyer and the presale
        const buyerFinalEtherBalance = await getBalance(accounts[1]);
        const presaleFinalEtherBalance = await getBalance(cryptoequityPresale.address);

        // get the final token balances of the buyer and the presale
        const buyerFinalTokenBalance = await cryptoequityToken.balanceOf(accounts[1]);
        const presaleFinalTokenBalance = await cryptoequityToken.balanceOf(cryptoequityPresale.address);

        // check the buyer ether balance
        const buyerEtherBalanceDiff = buyerFinalEtherBalance.minus(buyerInitialEtherBalance);
        assert.strictEqual(buyerEtherBalanceDiff.toNumber(), -50 - 90921, "buyer had wrong ether balance");

        // check the presale ether balance
        const presaleEtherBalanceDiff = presaleFinalEtherBalance.minus(presaleInitialEtherBalance);
        assert.strictEqual(presaleEtherBalanceDiff.toNumber(), 50, "presale had wrong ether balance");

        // check the buyer token balance
        const buyerTokenBalanceDiff = buyerFinalTokenBalance.minus(buyerInitialTokenBalance);
        assert.strictEqual(buyerTokenBalanceDiff.toNumber(), 1, "buyer had wrong token balance");

        // check the presale token balance
        const presaleTokenBalanceDiff = presaleFinalTokenBalance.minus(presaleInitialTokenBalance);
        assert.strictEqual(presaleTokenBalanceDiff.toNumber(), -1, "presale had wrong token balance");
    })

    it("overpaying for all tokens refunds extra ether", async () => {

        // get the initial ether balances of the buyer and the presale
        const buyerInitialEtherBalance = await getBalance(accounts[1]);
        const presaleInitialEtherBalance = await getBalance(cryptoequityPresale.address);

        // get the initial token balances of the buyer and the presale
        const buyerInitialTokenBalance = await cryptoequityToken.balanceOf(accounts[1]);
        const presaleInitialTokenBalance = await cryptoequityToken.balanceOf(cryptoequityPresale.address);

        // buy all tokens and send an extra 1000000000000000 wei
	await sendTransaction({to: cryptoequityPresale.address, from: accounts[1], value: 8087499999999950, gasPrice: gasPrice, gas: gas});

        // get the final ether balances of the buyer and the presale
        const buyerFinalEtherBalance = await getBalance(accounts[1]);
        const presaleFinalEtherBalance = await getBalance(cryptoequityPresale.address);

        // get the final token balances of the buyer and the presale
        const buyerFinalTokenBalance = await cryptoequityToken.balanceOf(accounts[1]);
        const presaleFinalTokenBalance = await cryptoequityToken.balanceOf(cryptoequityPresale.address);

        // check the buyer ether balance
        const buyerEtherBalanceDiff = buyerFinalEtherBalance.minus(buyerInitialEtherBalance);
        assert.strictEqual(buyerEtherBalanceDiff.toNumber(), -7087499999999950 - 107623, "buyer had wrong ether balance");

        // check the presale ether balance
        const presaleEtherBalanceDiff = presaleFinalEtherBalance.minus(presaleInitialEtherBalance);
        assert.strictEqual(presaleEtherBalanceDiff.toNumber(), 7087499999999950, "presale had wrong ether balance");

        // check the buyer token balance
        const buyerTokenBalanceDiff = buyerFinalTokenBalance.minus(buyerInitialTokenBalance);
        assert.strictEqual(buyerTokenBalanceDiff.toNumber(), 89999999999999, "buyer had wrong token balance");

        // check the presale token balance
        const presaleTokenBalanceDiff = presaleFinalTokenBalance.minus(presaleInitialTokenBalance);
        assert.strictEqual(presaleTokenBalanceDiff.toNumber(), -89999999999999, "presale had wrong token balance");
    });
});
