var CryptoequityToken = artifacts.require("./CryptoequityToken.sol");

contract('CryptoequityToken', function(accounts) {

    /*************/
    /* Utilities */
    /*************/

    // before each test, initialize the cryptoequityToken variable from the deployed contract
    beforeEach(async () => {
        cryptoequityToken = await CryptoequityToken.deployed()
    })

    const promisify = (inner) =>
      new Promise((resolve, reject) =>
        inner((err, res) => {
          if (err) { reject(err) }
          resolve(res);
        })
      );
    
    const sendTransaction = (data) =>
      promisify(cb => web3.eth.sendTransaction(data, cb));

    const gasPrice = 10000000000;
    const gas = 100000;

    /******************/
    /* Creation Tests */
    /******************/

    it("initial supply should be allocated to first account", async () => {
        const firstAccountBalance = await cryptoequityToken.balanceOf.call(accounts[0])
        assert.strictEqual(firstAccountBalance.toNumber(), 90000000000000, "first account had incorrect balance")
    });

    it("no tokens should be allocated to other accounts", async () => {
        const secondAccountBalance = await cryptoequityToken.balanceOf.call(accounts[1])
        assert.strictEqual(secondAccountBalance.toNumber(), 0, "second account had incorrect balance")
    });

    it("token should have the correct metadata", async () => {
        const name = await cryptoequityToken.name.call()
        assert.strictEqual(name, 'Test Cryptoequity Token')

        const symbol = await cryptoequityToken.symbol.call()
        assert.strictEqual(symbol, 'TCT')

        const decimals = await cryptoequityToken.decimals.call()
        assert.strictEqual(decimals.toNumber(), 6)

        const totalSupply = await cryptoequityToken.totalSupply.call()
        assert.strictEqual(totalSupply.toNumber(), 90000000000000)
    })

    it("token contract not accept ether without a function call", async () => {
        
        let didThrow = false;
        try {
            await sendTransaction({from: accounts[0], to: cryptoequityToken.address, value: 1, gasPrice: gasPrice, gas: gas})
        } catch (e) {
            //didThrow = (e.message == "VM Exception while processing transaction: invalid opcode")
            didThrow = true; //(e.message == "VM Exception while processing transaction: invalid opcode")
        }

        assert.isTrue(didThrow, "failed to throw when sending ether to token contract");
    })

    // TODO
    /*
    it("transfer event fires correctly on contract creation", async () => {
        console.log(cryptoequityToken.Transfer())
        console.log(await cryptoequityToken.Transfer().implementation.getLogs({fromBlock: 'earliest', toBlock: 'latest'}))
    })
    */

    /******************/
    /* Transfer Tests */
    /******************/

    it("first account should send 500k tokens to the second account", async () => {
        const transfer = await cryptoequityToken.transfer(accounts[1], 500000000000, {from: accounts[0], gasPrice: gasPrice})

        const transferLog = transfer.logs.find(element => element.event.match('Transfer'))
        assert.strictEqual(transferLog.args._from, accounts[0], "Transfer event had incorrect _from")
        assert.strictEqual(transferLog.args._to, accounts[1], "Transfer event had incorrect _to")
        assert.strictEqual(transferLog.args._value.toNumber(), 500000000000, "Transfer event had incorrect _value")

        const firstAccountBalance = await cryptoequityToken.balanceOf.call(accounts[0])
        assert.strictEqual(firstAccountBalance.toNumber(), 89500000000000, "first account had incorrect balance")

        const secondAccountBalance = await cryptoequityToken.balanceOf.call(accounts[1])
        assert.strictEqual(secondAccountBalance.toNumber(), 500000000000, "second account had incorrect balance")
    })

    it("second account should fail to send 600k tokens to the third account", async () => {
        let didThrow = false;

        try {
            await cryptoequityToken.transfer(accounts[2], 600000000000, {from: accounts[1], gasPrice: gasPrice, gas: gas})
        } catch (e) {
            //didThrow = (e.message == "VM Exception while processing transaction: invalid opcode")
            didThrow = true; //(e.message == "VM Exception while processing transaction: invalid opcode")
        }

        assert.isTrue(didThrow, "failed to throw when sending 600k tokens");
    })

    it("second account should send 200k tokens to the third account", async () => {
        const transfer = await cryptoequityToken.transfer(accounts[2], 200000000000, {from: accounts[1], gasPrice: gasPrice})

        const transferLog = transfer.logs.find(element => element.event.match('Transfer'))
        assert.strictEqual(transferLog.args._from, accounts[1], "Transfer event had incorrect _from")
        assert.strictEqual(transferLog.args._to, accounts[2], "Transfer event had incorrect _to")
        assert.strictEqual(transferLog.args._value.toNumber(), 200000000000, "Transfer event had incorrect _value")

        const secondAccountBalance = await cryptoequityToken.balanceOf.call(accounts[1])
        assert.strictEqual(secondAccountBalance.toNumber(), 300000000000, "second account had incorrect balance")

        const thirdAccountBalance = await cryptoequityToken.balanceOf.call(accounts[2])
        assert.strictEqual(thirdAccountBalance.toNumber(), 200000000000, "third account had incorrect balance")
    })

    it("third account should send zero tokens to the fourth account", async () => {
        const transfer = await cryptoequityToken.transfer(accounts[3], 0, {from: accounts[2], gasPrice: gasPrice})

        const transferLog = transfer.logs.find(element => element.event.match('Transfer'))
        assert.strictEqual(transferLog.args._from, accounts[2], "Transfer event had incorrect _from")
        assert.strictEqual(transferLog.args._to, accounts[3], "Transfer event had incorrect _to")
        assert.strictEqual(transferLog.args._value.toNumber(), 0, "Transfer event had incorrect _value")

        const thirdAccountBalance = await cryptoequityToken.balanceOf.call(accounts[2])
        assert.strictEqual(thirdAccountBalance.toNumber(), 200000000000, "third account had incorrect balance")

        const fourthAccountBalance = await cryptoequityToken.balanceOf.call(accounts[3])
        assert.strictEqual(fourthAccountBalance.toNumber(), 0, "fourth account had incorrect balance")
    })

    /******************/
    /* Approval Tests */
    /******************/

    it("third account should approve 100k tokens for the fourth account", async () => {
        const approval = await cryptoequityToken.approve(accounts[3], 100000000000, {from: accounts[2], gasPrice: gasPrice})

        const approvalLog = approval.logs.find(element => element.event.match('Approval'))
        assert.strictEqual(approvalLog.args._owner, accounts[2], "Approval event had incorrect _owner")
        assert.strictEqual(approvalLog.args._spender, accounts[3], "Approval event had incorrect _spender")
        assert.strictEqual(approvalLog.args._value.toNumber(), 100000000000, "Approval event had incorrect _value")

        const allowance = await cryptoequityToken.allowance.call(accounts[2], accounts[3])
        assert.strictEqual(allowance.toNumber(), 100000000000, "allowance for fourth account was incorrect")
    })

    it("fourth account should fail to transfer 200k tokens to fifth account from third account", async () => {
        let didThrow = false;

        try {
            await cryptoequityToken.transferFrom(accounts[2], accounts[4], 200000000000, {from: accounts[3], gasPrice: gasPrice, gas: gas})
        } catch (e) {
            //didThrow = (e.message == "VM Exception while processing transaction: invalid opcode")
            didThrow = true; //(e.message == "VM Exception while processing transaction: invalid opcode")
        }

        assert.isTrue(didThrow, "failed to throw when transferring 200k tokens");
    })

    it("fourth account should transfer 50k tokens to fifth account from third account", async () => {
        const transferFrom = await cryptoequityToken.transferFrom(accounts[2], accounts[4], 50000000000, {from: accounts[3], gasPrice: gasPrice})

        const transferFromLog = transferFrom.logs.find(element => element.event.match('Transfer'))
        assert.strictEqual(transferFromLog.args._from, accounts[2], "Transfer event had incorrect _from")
        assert.strictEqual(transferFromLog.args._to, accounts[4], "Transfer event had incorrect _to")
        assert.strictEqual(transferFromLog.args._value.toNumber(), 50000000000, "Transfer event had incorrect _value")

        const thirdAccountBalance = await cryptoequityToken.balanceOf.call(accounts[2])
        assert.strictEqual(thirdAccountBalance.toNumber(), 150000000000, "third account had incorrect balance")

        const fifthAccountBalance = await cryptoequityToken.balanceOf.call(accounts[4])
        assert.strictEqual(fifthAccountBalance.toNumber(), 50000000000, "fifth account had incorrect balance")

        const fourthAccountAllowance = await cryptoequityToken.allowance.call(accounts[2], accounts[3])
        assert.strictEqual(fourthAccountAllowance.toNumber(), 50000000000, "allowance for fourth account was incorrect")
    })

    it("third account should update approval to 100k tokens for the fourth account", async () => {
        const approval = await cryptoequityToken.approve(accounts[3], 100000000000, {from: accounts[2], gasPrice: gasPrice})

        const approvalLog = approval.logs.find(element => element.event.match('Approval'))
        assert.strictEqual(approvalLog.args._owner, accounts[2], "Approval event had incorrect _owner")
        assert.strictEqual(approvalLog.args._spender, accounts[3], "Approval event had incorrect _spender")
        assert.strictEqual(approvalLog.args._value.toNumber(), 100000000000, "Approval event had incorrect _value")

        const fourthAccountAllowance = await cryptoequityToken.allowance.call(accounts[2], accounts[3])
        assert.strictEqual(fourthAccountAllowance.toNumber(), 100000000000, "allowance for fourth account was incorrect")
    })

    it("fourth account should fail to transfer 200k tokens to fifth account from third account", async () => {
        let didThrow = false;

        try {
            await cryptoequityToken.transferFrom(accounts[2], accounts[4], 200000000000, {from: accounts[3], gasPrice: gasPrice, gas: gas})
        } catch (e) {
            //didThrow = (e.message == "VM Exception while processing transaction: invalid opcode")
            didThrow = true; //(e.message == "VM Exception while processing transaction: invalid opcode")
        }

        assert.isTrue(didThrow, "failed to throw when transferring 200k tokens");
    })

    it("fourth account should transfer 100k tokens to fifth account from third account", async () => {
        const transferFrom = await cryptoequityToken.transferFrom(accounts[2], accounts[4], 100000000000, {from: accounts[3], gasPrice: gasPrice})

        const transferFromLog = transferFrom.logs.find(element => element.event.match('Transfer'))
        assert.strictEqual(transferFromLog.args._from, accounts[2], "Transfer event had incorrect _from")
        assert.strictEqual(transferFromLog.args._to, accounts[4], "Transfer event had incorrect _to")
        assert.strictEqual(transferFromLog.args._value.toNumber(), 100000000000, "Transfer event had incorrect _value")

        const thirdAccountBalance = await cryptoequityToken.balanceOf.call(accounts[2])
        assert.strictEqual(thirdAccountBalance.toNumber(), 50000000000, "third account had incorrect balance")

        const fifthAccountBalance = await cryptoequityToken.balanceOf.call(accounts[4])
        assert.strictEqual(fifthAccountBalance.toNumber(), 150000000000, "fifth account had incorrect balance")

        const fourthAccountAllowance = await cryptoequityToken.allowance.call(accounts[2], accounts[3])
        assert.strictEqual(fourthAccountAllowance.toNumber(), 0, "allowance for fourth account was incorrect")
    })

    it("fifth account should fail to transfer 50k tokens to fifth account from third account", async () => {
        let didThrow = false;

        try {
            await cryptoequityToken.transferFrom(accounts[2], accounts[4], 50000000000, {from: accounts[4], gasPrice: gasPrice, gas: gas})
        } catch (e) {
            //didThrow = (e.message == "VM Exception while processing transaction: invalid opcode")
            didThrow = true; //(e.message == "VM Exception while processing transaction: invalid opcode")
        }

        assert.isTrue(didThrow, "failed to throw when transferring 50k tokens");
    })

    it("fifth account should increase approval by 100k tokens for the third account", async () => {
        const approval = await cryptoequityToken.increaseApproval(accounts[2], 100000000000, {from: accounts[4], gasPrice: gasPrice})

        const approvalLog = approval.logs.find(element => element.event.match('Approval'))
        assert.strictEqual(approvalLog.args._owner, accounts[4], "Approval event had incorrect _owner")
        assert.strictEqual(approvalLog.args._spender, accounts[2], "Approval event had incorrect _spender")
        assert.strictEqual(approvalLog.args._value.toNumber(), 100000000000, "Approval event had incorrect _value")

        const thirdAccountAllowance = await cryptoequityToken.allowance.call(accounts[4], accounts[2])
        assert.strictEqual(thirdAccountAllowance.toNumber(), 100000000000, "allowance for third account was incorrect")
    })

    it("fifth account should increase approval by 50k tokens for the third account", async () => {
        const approval = await cryptoequityToken.increaseApproval(accounts[2], 50000000000, {from: accounts[4], gasPrice: gasPrice})

        const approvalLog = approval.logs.find(element => element.event.match('Approval'))
        assert.strictEqual(approvalLog.args._owner, accounts[4], "Approval event had incorrect _owner")
        assert.strictEqual(approvalLog.args._spender, accounts[2], "Approval event had incorrect _spender")
        assert.strictEqual(approvalLog.args._value.toNumber(), 150000000000, "Approval event had incorrect _value")

        const thirdAccountAllowance = await cryptoequityToken.allowance.call(accounts[4], accounts[2])
        assert.strictEqual(thirdAccountAllowance.toNumber(), 150000000000, "allowance for third account was incorrect")
    })

    it("fifth account should decrease approval by 50k tokens for the third account", async () => {
        const approval = await cryptoequityToken.decreaseApproval(accounts[2], 50000000000, {from: accounts[4], gasPrice: gasPrice})

        const approvalLog = approval.logs.find(element => element.event.match('Approval'))
        assert.strictEqual(approvalLog.args._owner, accounts[4], "Approval event had incorrect _owner")
        assert.strictEqual(approvalLog.args._spender, accounts[2], "Approval event had incorrect _spender")
        assert.strictEqual(approvalLog.args._value.toNumber(), 100000000000, "Approval event had incorrect _value")

        const thirdAccountAllowance = await cryptoequityToken.allowance.call(accounts[4], accounts[2])
        assert.strictEqual(thirdAccountAllowance.toNumber(), 100000000000, "allowance for third account was incorrect")
    })

    it("fifth account should decrease approval by 200k tokens for the third account", async () => {
        const approval = await cryptoequityToken.decreaseApproval(accounts[2], 200000000000, {from: accounts[4], gasPrice: gasPrice})

        const approvalLog = approval.logs.find(element => element.event.match('Approval'))
        assert.strictEqual(approvalLog.args._owner, accounts[4], "Approval event had incorrect _owner")
        assert.strictEqual(approvalLog.args._spender, accounts[2], "Approval event had incorrect _spender")
        assert.strictEqual(approvalLog.args._value.toNumber(), 0, "Approval event had incorrect _value")

        const thirdAccountAllowance = await cryptoequityToken.allowance.call(accounts[4], accounts[2])
        assert.strictEqual(thirdAccountAllowance.toNumber(), 0, "allowance for third account was incorrect")
    })
});
