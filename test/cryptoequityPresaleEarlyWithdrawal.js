var Web3 = require('web3');
var BN = require('bignumber.js');
var sleep = require('sleep');

var CryptoequityPresale = artifacts.require("./CryptoequityPresale.sol");
var CryptoequityToken = artifacts.require("./CryptoequityToken.sol");



contract('CryptoequityPresaleEarlyWithdrawal', function(accounts) {

    /*************/
    /* Utilities */
    /*************/

    // before each test, initialize the cryptoequityToken and cryptoequityPresale variables from the deployed contracts
    beforeEach(async () => {
	cryptoequityPresale = await CryptoequityPresale.deployed();
        cryptoequityToken = await CryptoequityToken.deployed();
    })

    const promisify = (inner) =>
      new Promise((resolve, reject) =>
        inner((err, res) => {
          if (err) { reject(err) }
          resolve(res);
        })
      );
    
    const sendTransaction = (data) =>
      promisify(cb => web3.eth.sendTransaction(data, cb));


    const getBalance = (address) =>
      promisify(cb => web3.eth.getBalance(address, cb));

    //const gasPrice = 10000000000;
    const gasPrice = 1;
    const gas = 100000;


    /************************/
    /* Emergency User Tests */
    /************************/
    it("send initial tokens and wait until first round starts", async () => {
        console.log('Waiting for first round...');
        const firstRoundStart = await cryptoequityPresale.roundTimestamps.call(0)
        while (Math.floor(Date.now()/1000) <= firstRoundStart.valueOf()) {};
        console.log('First round started.');

        cryptoequityToken.transfer(cryptoequityPresale.address, 90000000000000, {from: accounts[0], gasPrice: gasPrice});

        const isStarted = await cryptoequityPresale.isStarted.call();

	// Buy 1ETH of tokens so that presale contract has some ether
	await sendTransaction({to: cryptoequityPresale.address, from: accounts[1], value: 1000000000000, gasPrice: gasPrice});
    })

    it("withdrawEther successfully withdraws ether", async () => {
	let presaleController = await cryptoequityPresale.presaleController.call();

        // get the initial ether balances of the controller and the presale
        const controllerInitialEtherBalance = await getBalance(presaleController);
        const presaleInitialEtherBalance = await getBalance(cryptoequityPresale.address);

        // call withdrawEther
        await cryptoequityPresale.withdrawEther({from: presaleController, gasPrice: gasPrice});

        // get the final ether balances of the controller and the presale
        const controllerFinalEtherBalance = await getBalance(presaleController);
        const presaleFinalEtherBalance = await getBalance(cryptoequityPresale.address);

        // check the controller ether balance
        const controllerEtherBalanceDiff = controllerFinalEtherBalance.minus(controllerInitialEtherBalance);
        assert.strictEqual(controllerEtherBalanceDiff.toNumber(), 1000000000000 - 30185, "controller had wrong ether balance");

        // check the presale ether balance
        const presaleEtherBalanceDiff = presaleFinalEtherBalance.minus(presaleInitialEtherBalance);
        assert.strictEqual(presaleEtherBalanceDiff.toNumber(), -1000000000000, "presale had wrong ether balance");
    });

    it("withdrawTokens successfully withdraws tokens", async () => {
	let presaleController = await cryptoequityPresale.presaleController.call();

        // get the initial token balances of the controller and the presale
        const controllerInitialTokenBalance = await cryptoequityToken.balanceOf(presaleController);
        const presaleInitialTokenBalance = await cryptoequityToken.balanceOf(cryptoequityPresale.address);

        // call withdrawTokens for 9980000000000 tokens (leaving the contract with 80000000000000
        await cryptoequityPresale.withdrawTokens(9980000000000, {from: presaleController, gasPrice: gasPrice});

        // get the final token balances of the controller and the presale
        const controllerFinalTokenBalance = await cryptoequityToken.balanceOf(presaleController);
        const presaleFinalTokenBalance = await cryptoequityToken.balanceOf(cryptoequityPresale.address);

        // check the controller token balance
        const controllerTokenBalanceDiff = controllerFinalTokenBalance.minus(controllerInitialTokenBalance);
        assert.strictEqual(controllerTokenBalanceDiff.toNumber(), 9980000000000, "controller had wrong token balance");

        // check the presale token balance
        const presaleTokenBalanceDiff = presaleFinalTokenBalance.minus(presaleInitialTokenBalance);
        assert.strictEqual(presaleTokenBalanceDiff.toNumber(), -9980000000000, "presale had wrong token balance");
    });

    it("presale continues after withdrawing ether and tokens", async () => {

        const buyerInitialEtherBalance = await getBalance(accounts[1]);
        const buyerInitialTokenBalance = await cryptoequityToken.balanceOf(accounts[1]);

	// buy another 1 ETH of tokens from accounts[1]
	await sendTransaction({to: cryptoequityPresale.address, from: accounts[1], value: 1000000000000, gasPrice: gasPrice});

        const buyerFinalEtherBalance = await getBalance(accounts[1]);
        const buyerFinalTokenBalance = await cryptoequityToken.balanceOf(accounts[1]);

        const buyerEtherBalanceDiff = buyerFinalEtherBalance.minus(buyerInitialEtherBalance);
        assert.strictEqual(buyerEtherBalanceDiff.toNumber(), -1000000000000 - 69221, "buyer had wrong ether balance");

        const buyerTokenBalanceDiff = buyerFinalTokenBalance.minus(buyerInitialTokenBalance);
        assert.strictEqual(buyerTokenBalanceDiff.toNumber(), 20000000000, "buyer had wrong token balance");

    });

    it("wait until the end of the presale", async () => {

        console.log('Waiting for end of presale...');
        const endTimestamp = await cryptoequityPresale.endTimestamp.call()
        while (Math.floor(Date.now()/1000) <= endTimestamp.valueOf()) {};
        console.log('Presale ended.');

        await sendTransaction({from: accounts[0], to: accounts[1], value: 1, gasPrice: gasPrice})

        const isEnded = await cryptoequityPresale.isEnded.call()
        assert.isTrue(isEnded, "presale has not yet ended")
    });

    it("remaining ether/tokens can be withdrawn after presale has ended", async () => {

        const controllerInitialEtherBalance = await getBalance(accounts[0]);
        await cryptoequityPresale.finalizePresale({from: accounts[0], gasPrice: gasPrice});
        const controllerFinalEtherBalance = await getBalance(accounts[0]);

        const controllerEtherBalanceDiff = controllerFinalEtherBalance.minus(controllerInitialEtherBalance);
        assert.strictEqual(controllerEtherBalanceDiff.toNumber(),1000000000000 - 34207, "controller had wrong balance");

        const controllerTokenBalance = await cryptoequityToken.balanceOf(accounts[0]);
        assert.strictEqual(controllerTokenBalance.toNumber(), 89960000000000, "controller had wrong token balance");

        const investorTokenBalance = await cryptoequityToken.balanceOf(accounts[1]);
        assert.strictEqual(investorTokenBalance.toNumber(), 40000000000, "investor had wrong token balance");

        const presaleContractEtherBalance = await getBalance(cryptoequityPresale.address);
        assert.strictEqual(presaleContractEtherBalance.toNumber(), 0, "presale contract had wrong ether balance");

        const presaleContractTokenBalance = await cryptoequityToken.balanceOf(cryptoequityPresale.address);
        assert.strictEqual(presaleContractTokenBalance.toNumber(), 0, "presale contract had wrong token balance");
    });
});
