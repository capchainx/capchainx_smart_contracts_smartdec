var Web3 = require('web3');
var BN = require('bignumber.js');
var sleep = require('sleep');

var CryptoequityPresale = artifacts.require("./CryptoequityPresale.sol");
var CryptoequityToken = artifacts.require("./CryptoequityToken.sol");



contract('CryptoequityPresaleMalicious', function(accounts) {
    /*************/
    /* Utilities */
    /*************/

    // before each test, initialize the cryptoequityToken and cryptoequityPresale variables from the deployed contracts
    beforeEach(async () => {
	cryptoequityPresale = await CryptoequityPresale.deployed();
        cryptoequityToken = await CryptoequityToken.deployed();
    })

    const promisify = (inner) =>
      new Promise((resolve, reject) =>
        inner((err, res) => {
          if (err) { reject(err) }
          resolve(res);
        })
      );
    
    const sendTransaction = (data) =>
      promisify(cb => web3.eth.sendTransaction(data, cb));

    /************************/
    /* Malicious User Tests */
    /************************/
    it("send initial tokens and wait until first round starts", async () => {
        console.log('Waiting for first round...');
        const firstRoundStart = await cryptoequityPresale.roundTimestamps.call(0)
        while (Math.floor(Date.now()/1000) <= firstRoundStart.valueOf()) {};
        console.log('First round started.');

        cryptoequityToken.transfer(cryptoequityPresale.address, 1000000000000, {from: accounts[0]});

        const isStarted = await cryptoequityPresale.isStarted.call();
    });
    
    it("Send zero ether", async () => {
        try {
            await sendTransaction({to: cryptoequityPresale.address, from: accounts[1], value: 0});
        } catch (e) {
            didThrow = (e.message == "VM Exception while processing transaction: invalid opcode");
        }

        assert.isTrue(didThrow, "failed to throw after buyTokens with 0 Wei");
    });

    it("Request more tokens than are available", async () => {
       const priceForTokens = await cryptoequityPresale.priceForTokens.call(1000000000000);
       try {
            await sendTransaction({to: cryptoequityPresale.address, from: accounts[1], value: priceForTokens.toNumber() + 100});
        } catch (e) {
            didThrow = (e.message == "VM Exception while processing transaction: invalid opcode");
        }

        assert.isTrue(didThrow, "failed to throw after purchasing more tokens than available");
    });

    it("Can't call declareEmergency", async () => {
	var didThrow = false;

        try {
	    await cryptoequityPresale.declareEmergency({from: accounts[1]});
	} catch (e) {
	    didThrow = (e.message == "VM Exception while processing transaction: invalid opcode")
	}

	assert.isTrue(didThrow, "failed to throw after calling declareEmergency without using the controller address");
    });

    it("Can't call finalizePresale", async () => {
	var didThrow = false;

	// Wait until end of presale
        console.log('Waiting for end of presale...');
        const endTimestamp = await cryptoequityPresale.endTimestamp.call();
        while (Math.floor(Date.now()/1000) <= endTimestamp.valueOf()) {};
        console.log('Presale ended.');
	
        try {
	    await cryptoequityPresale.finalizePresale({from: accounts[1]});
	} catch (e) {
	    didThrow = (e.message == "VM Exception while processing transaction: invalid opcode")
	}

	assert.isTrue(didThrow, "failed to throw after calling declareEmergency without using the controller address");
    });
});
