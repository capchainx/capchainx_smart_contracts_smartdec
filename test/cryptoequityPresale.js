var sleep = require('sleep');

var CryptoequityToken = artifacts.require("./CryptoequityToken.sol");
var CryptoequityPresale = artifacts.require("./CryptoequityPresale.sol");

contract('CryptoequityPresale', function(accounts) {

    /*************/
    /* Utilities */
    /*************/

    // before each test, initialize the cryptoequityToken and cryptoequityPresale variables from the deployed contracts
    beforeEach(async () => {
        cryptoequityToken = await CryptoequityToken.deployed()
        cryptoequityPresale = await CryptoequityPresale.deployed()
    })

    const promisify = (inner) =>
      new Promise((resolve, reject) =>
        inner((err, res) => {
          if (err) { reject(err) }
          resolve(res);
        })
      );
    
    const sendTransaction = (data) =>
      promisify(cb => web3.eth.sendTransaction(data, cb));
    
    const getBalance = (address) =>
      promisify(cb => web3.eth.getBalance(address, cb));

    //const gasPrice = 10000000000;
    const gasPrice = 1;
    const gas = 100000;


    /*****************/
    /* Initial Tests */
    /*****************/

    it("presale contract should be initialized with correct parameters", async () => {

        const controllerAddress = await cryptoequityPresale.presaleController.call();
        assert.strictEqual(controllerAddress, accounts[0], "Controller address was incorrect");

        const tokenAddress = await cryptoequityPresale.cryptoequityToken.call();
        assert.strictEqual(tokenAddress, cryptoequityToken.address, "Token address was incorrect");

        const basePrice = await cryptoequityPresale.basePrice.call();
        assert.equal(basePrice, 100, "Base price was incorrect");
    });

    it("send 90 million cryptoequity tokens from controller address to smart contract", async () => {

        await cryptoequityToken.transfer(cryptoequityPresale.address, 90000000000000, {from: accounts[0], gasPrice: gasPrice});

        const presaleBalance = await cryptoequityToken.balanceOf.call(cryptoequityPresale.address);
        assert.strictEqual(presaleBalance.toNumber(), 90000000000000, "presale contract balance was incorrect");

        const controllerBalance = await cryptoequityToken.balanceOf.call(accounts[0]);
        assert.strictEqual(controllerBalance.toNumber(), 0, "controller address balance was incorrect");
    });

    it("second account should not be able to purchase tokens before presale begins", async () => {

        let didThrow = false;
        try {
            await sendTransaction({to: cryptoequityPresale.address, from: accounts[1], value: 1000000000000, gasPrice: gasPrice, gas: gas});
        } catch (e) {
            didThrow = (e.message == "VM Exception while processing transaction: invalid opcode")
        }

        assert.isTrue(didThrow, "failed to throw when buying tokens after presale ended");
    });


    /*********************/
    /* First Round Tests */
    /*********************/

    it("wait until the first round", async () => {

        console.log('Waiting for first round...');
        const firstRoundStart = await cryptoequityPresale.roundTimestamps.call(0)
        while (Math.floor(Date.now()/1000) <= firstRoundStart.valueOf()) {};
        console.log('First round started.');

        await sendTransaction({from: accounts[0], to: accounts[1], value: 1, gasPrice: gasPrice})

        const isStarted = await cryptoequityPresale.isStarted.call()
        assert.isTrue(isStarted, "first round has not yet started")
    });

    it("priceForTokens should behave correctly in first round (time based)", async () => {

        let priceForTokens = await cryptoequityPresale.priceForTokens.call(1000000);
        assert.strictEqual(priceForTokens.toNumber(), 50000000, "price for 1 token is incorrect");

        priceForTokens = await cryptoequityPresale.priceForTokens.call(1000000000000);
        assert.strictEqual(priceForTokens.toNumber(), 50000000000000, "price for 1m token is incorrect");

        priceForTokens = await cryptoequityPresale.priceForTokens.call(22500000000000);
        assert.strictEqual(priceForTokens.toNumber(), 1125000000000000, "price for 22.5m tokens is incorrect");

        priceForTokens = await cryptoequityPresale.priceForTokens.call(22500000000001);
        assert.strictEqual(priceForTokens.toNumber(), 1125000000000075, "price for 22.5m and 1 tokens is incorrect");

        priceForTokens = await cryptoequityPresale.priceForTokens.call(45000000000000);
        assert.strictEqual(priceForTokens.toNumber(), 2812500000000000, "price for 45m tokens is incorrect");

        priceForTokens = await cryptoequityPresale.priceForTokens.call(45000000000001);
        assert.strictEqual(priceForTokens.toNumber(), 2812500000000090, "price for 45m and 1 tokens is incorrect");

        priceForTokens = await cryptoequityPresale.priceForTokens.call(67500000000000);
        assert.strictEqual(priceForTokens.toNumber(), 4837500000000000, "price for 67.5m tokens is incorrect");

        priceForTokens = await cryptoequityPresale.priceForTokens.call(67500000000001);
        assert.strictEqual(priceForTokens.toNumber(), 4837500000000100, "price for 67.5m and 1 tokens is incorrect");

        priceForTokens = await cryptoequityPresale.priceForTokens.call(90000000000000);
        assert.strictEqual(priceForTokens.toNumber(), 7087500000000000, "price for 90m tokens is incorrect");
    });

    it("tokensForPrice should behave correctly in first round (time based)", async () => {

        let tokensForPrice = await cryptoequityPresale.tokensForPrice.call(49);
        assert.strictEqual(tokensForPrice.toNumber(), 0, "tokens for 49 wei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(50);
        assert.strictEqual(tokensForPrice.toNumber(), 1, "tokens for 50 wei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(51);
        assert.strictEqual(tokensForPrice.toNumber(), 1, "tokens for 51 wei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(99);
        assert.strictEqual(tokensForPrice.toNumber(), 1, "tokens for 99 wei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(100);
        assert.strictEqual(tokensForPrice.toNumber(), 2, "tokens for 100 wei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(101);
        assert.strictEqual(tokensForPrice.toNumber(), 2, "tokens for 101 wei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(1000000000);
        assert.strictEqual(tokensForPrice.toNumber(), 20000000, "tokens for 1 gwei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(50000000000000);
        assert.strictEqual(tokensForPrice.toNumber(), 1000000000000, "tokens for 50 twei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(1125000000000000);
        assert.strictEqual(tokensForPrice.toNumber(), 22500000000000, "tokens for 1125 twei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(1125000000000074);
        assert.strictEqual(tokensForPrice.toNumber(), 22500000000000, "tokens for 1125 twei + 74 is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(1125000000000075);
        assert.strictEqual(tokensForPrice.toNumber(), 22500000000001, "tokens for 1125 twei + 75 is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(1125000000000149);
        assert.strictEqual(tokensForPrice.toNumber(), 22500000000001, "tokens for 1125 twei + 149 is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(1125000000000150);
        assert.strictEqual(tokensForPrice.toNumber(), 22500000000002, "tokens for 1125 twei + 150 is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(2812500000000000);
        assert.strictEqual(tokensForPrice.toNumber(), 45000000000000, "tokens for 2812.5 twei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(2812500000000089);
        assert.strictEqual(tokensForPrice.toNumber(), 45000000000000, "tokens for 2812.5 twei + 89 is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(2812500000000090);
        assert.strictEqual(tokensForPrice.toNumber(), 45000000000001, "tokens for 2812.5 twei + 90 is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(2812500000000179);
        assert.strictEqual(tokensForPrice.toNumber(), 45000000000001, "tokens for 2812.5 twei + 179 is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(2812500000000180);
        assert.strictEqual(tokensForPrice.toNumber(), 45000000000002, "tokens for 2812.5 twei + 180 is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(4837500000000000);
        assert.strictEqual(tokensForPrice.toNumber(), 67500000000000, "tokens for 4837.5 twei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(4837500000000099);
        assert.strictEqual(tokensForPrice.toNumber(), 67500000000000, "tokens for 4837.5 twei + 99 is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(4837500000000100);
        assert.strictEqual(tokensForPrice.toNumber(), 67500000000001, "tokens for 4837.5 twei + 100 is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(4837500000000199);
        assert.strictEqual(tokensForPrice.toNumber(), 67500000000001, "tokens for 4837.5 twei + 199 is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(4837500000000200);
        assert.strictEqual(tokensForPrice.toNumber(), 67500000000002, "tokens for 4837.5 twei + 200 is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(7087500000000000);
        assert.strictEqual(tokensForPrice.toNumber(), 90000000000000, "tokens for 7087.5 twei is incorrect");
    });

    it("second account should be able to buy 1 million tokens in first round (time based)", async () => {

        await sendTransaction({to: cryptoequityPresale.address, from: accounts[1], value: 50000000000049, gasPrice: gasPrice, gas: gas});

        const secondAccountBalance = await cryptoequityToken.balanceOf(accounts[1]);
        assert.strictEqual(secondAccountBalance.toNumber(), 1000000000000, "second account had incorrect balance");
        
        const presaleBalance = await cryptoequityToken.balanceOf(cryptoequityPresale.address);
        assert.strictEqual(presaleBalance.toNumber(), 89000000000000, "presale contract had incorrect balance");
    });


    /**********************/
    /* Second Round Tests */
    /**********************/

    it("wait until the second round", async () => {

        console.log('Waiting for second round...');
        const secondRoundStart = await cryptoequityPresale.roundTimestamps.call(1)
        while (Math.floor(Date.now()/1000) <= secondRoundStart.valueOf()) {};
        console.log('Second round started.');

        await sendTransaction({from: accounts[0], to: accounts[1], value: 1, gasPrice: gasPrice})
    });

    it("priceForTokens should behave correctly in second round (time based)", async () => {

        let priceForTokens = await cryptoequityPresale.priceForTokens.call(1000000);
        assert.strictEqual(priceForTokens.toNumber(), 75000000, "price for 1 token is incorrect");

        priceForTokens = await cryptoequityPresale.priceForTokens.call(1000000000000);
        assert.strictEqual(priceForTokens.toNumber(), 75000000000000, "price for 1m token is incorrect");

        priceForTokens = await cryptoequityPresale.priceForTokens.call(44000000000000);
        assert.strictEqual(priceForTokens.toNumber(), 3300000000000000, "price for 44m tokens is incorrect");

        priceForTokens = await cryptoequityPresale.priceForTokens.call(44000000000001);
        assert.strictEqual(priceForTokens.toNumber(), 3300000000000090, "price for 44m and 1 tokens is incorrect");

        priceForTokens = await cryptoequityPresale.priceForTokens.call(66500000000000);
        assert.strictEqual(priceForTokens.toNumber(), 5325000000000000, "price for 66.5m tokens is incorrect");

        priceForTokens = await cryptoequityPresale.priceForTokens.call(66500000000001);
        assert.strictEqual(priceForTokens.toNumber(), 5325000000000100, "price for 66.5m and 1 tokens is incorrect");

        priceForTokens = await cryptoequityPresale.priceForTokens.call(89000000000000);
        assert.strictEqual(priceForTokens.toNumber(), 7575000000000000, "price for 89m tokens is incorrect");
    });

    it("tokensForPrice should behave correctly in second round (time based)", async () => {

        let tokensForPrice = await cryptoequityPresale.tokensForPrice.call(74);
        assert.strictEqual(tokensForPrice.toNumber(), 0, "tokens for 74 wei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(75);
        assert.strictEqual(tokensForPrice.toNumber(), 1, "tokens for 75 wei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(76);
        assert.strictEqual(tokensForPrice.toNumber(), 1, "tokens for 76 wei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(149);
        assert.strictEqual(tokensForPrice.toNumber(), 1, "tokens for 149 wei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(150);
        assert.strictEqual(tokensForPrice.toNumber(), 2, "tokens for 150 wei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(151);
        assert.strictEqual(tokensForPrice.toNumber(), 2, "tokens for 151 wei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(3300000000000000);
        assert.strictEqual(tokensForPrice.toNumber(), 44000000000000, "tokens for 3300 twei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(3300000000000089);
        assert.strictEqual(tokensForPrice.toNumber(), 44000000000000, "tokens for 3300 twei + 89 is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(3300000000000090);
        assert.strictEqual(tokensForPrice.toNumber(), 44000000000001, "tokens for 3300 twei + 90 is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(3300000000000179);
        assert.strictEqual(tokensForPrice.toNumber(), 44000000000001, "tokens for 3300 twei + 179 is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(3300000000000180);
        assert.strictEqual(tokensForPrice.toNumber(), 44000000000002, "tokens for 3300 twei + 180 is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(5325000000000000);
        assert.strictEqual(tokensForPrice.toNumber(), 66500000000000, "tokens for 5325 twei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(5325000000000099);
        assert.strictEqual(tokensForPrice.toNumber(), 66500000000000, "tokens for 5325 twei + 99 is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(5325000000000100);
        assert.strictEqual(tokensForPrice.toNumber(), 66500000000001, "tokens for 5325 twei + 100 is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(5325000000000199);
        assert.strictEqual(tokensForPrice.toNumber(), 66500000000001, "tokens for 5325 twei + 199 is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(5325000000000200);
        assert.strictEqual(tokensForPrice.toNumber(), 66500000000002, "tokens for 5325 twei + 200 is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(7575000000000000);
        assert.strictEqual(tokensForPrice.toNumber(), 89000000000000, "tokens for 7575 twei is incorrect");
    });

    it("second account should be able to buy 1 million tokens in second round (time based)", async () => {

        await sendTransaction({to: cryptoequityPresale.address, from: accounts[1], value: 75000000000074, gasPrice: gasPrice});

        const secondAccountBalance = await cryptoequityToken.balanceOf(accounts[1]);
        assert.strictEqual(secondAccountBalance.toNumber(), 2000000000000, "second account had incorrect balance");
        
        const presaleBalance = await cryptoequityToken.balanceOf(cryptoequityPresale.address);
        assert.strictEqual(presaleBalance.toNumber(), 88000000000000, "presale contract had incorrect balance");
    });


    /**********************/
    /* Third Round Tests */
    /**********************/


    it("wait until the third round", async () => {

        console.log('Waiting for third round...');
        const thirdRoundStart = await cryptoequityPresale.roundTimestamps.call(2)
        while (Math.floor(Date.now()/1000) <= thirdRoundStart.valueOf()) {};
        console.log('Third round started.');

        await sendTransaction({from: accounts[0], to: accounts[1], value: 1, gasPrice: gasPrice})
    });

    it("priceForTokens should behave correctly in third round (time based)", async () => {

        let priceForTokens = await cryptoequityPresale.priceForTokens.call(1000000);
        assert.strictEqual(priceForTokens.toNumber(), 90000000, "price for 1 token is incorrect");

        priceForTokens = await cryptoequityPresale.priceForTokens.call(1000000000000);
        assert.strictEqual(priceForTokens.toNumber(), 90000000000000, "price for 1m token is incorrect");

        priceForTokens = await cryptoequityPresale.priceForTokens.call(65500000000000);
        assert.strictEqual(priceForTokens.toNumber(), 5895000000000000, "price for 65.5m tokens is incorrect");

        priceForTokens = await cryptoequityPresale.priceForTokens.call(65500000000001);
        assert.strictEqual(priceForTokens.toNumber(), 5895000000000100, "price for 65.5m and 1 tokens is incorrect");

        priceForTokens = await cryptoequityPresale.priceForTokens.call(88000000000000);
        assert.strictEqual(priceForTokens.toNumber(), 8145000000000000, "price for 88m tokens is incorrect");
    });

    it("tokensForPrice should behave correctly in third round (time based)", async () => {

        let tokensForPrice = await cryptoequityPresale.tokensForPrice.call(89);
        assert.strictEqual(tokensForPrice.toNumber(), 0, "tokens for 89 wei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(90);
        assert.strictEqual(tokensForPrice.toNumber(), 1, "tokens for 90 wei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(91);
        assert.strictEqual(tokensForPrice.toNumber(), 1, "tokens for 91 wei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(179);
        assert.strictEqual(tokensForPrice.toNumber(), 1, "tokens for 179 wei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(180);
        assert.strictEqual(tokensForPrice.toNumber(), 2, "tokens for 180 wei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(181);
        assert.strictEqual(tokensForPrice.toNumber(), 2, "tokens for 181 wei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(5895000000000000);
        assert.strictEqual(tokensForPrice.toNumber(), 65500000000000, "tokens for 5895 twei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(5895000000000099);
        assert.strictEqual(tokensForPrice.toNumber(), 65500000000000, "tokens for 5895 twei + 99 is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(5895000000000100);
        assert.strictEqual(tokensForPrice.toNumber(), 65500000000001, "tokens for 5895 twei + 100 is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(5895000000000199);
        assert.strictEqual(tokensForPrice.toNumber(), 65500000000001, "tokens for 5895 twei + 199 is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(5895000000000200);
        assert.strictEqual(tokensForPrice.toNumber(), 65500000000002, "tokens for 5895 twei + 200 is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(8145000000000000);
        assert.strictEqual(tokensForPrice.toNumber(), 88000000000000, "tokens for 8145 twei is incorrect");
    });

    it("second account should be able to buy 1 million tokens in third round (time based)", async () => {

        await sendTransaction({to: cryptoequityPresale.address, from: accounts[1], value: 90000000000089, gasPrice: gasPrice});

        const secondAccountBalance = await cryptoequityToken.balanceOf(accounts[1]);
        assert.strictEqual(secondAccountBalance.toNumber(), 3000000000000, "second account had incorrect balance");
        
        const presaleBalance = await cryptoequityToken.balanceOf(cryptoequityPresale.address);
        assert.strictEqual(presaleBalance.toNumber(), 87000000000000, "presale contract had incorrect balance");
    });


    /**********************/
    /* Fourth Round Tests */
    /**********************/

    it("wait until the fourth round", async () => {

        console.log('Waiting for fourth round...');
        const fourthRoundStart = await cryptoequityPresale.roundTimestamps.call(3)
        while (Math.floor(Date.now()/1000) <= fourthRoundStart.valueOf()) {};
        console.log('Fourth round started.');

        await sendTransaction({from: accounts[0], to: accounts[1], value: 1, gasPrice: gasPrice})
    });

    it("priceForTokens should behave correctly in fourth round (time based)", async () => {

        let priceForTokens = await cryptoequityPresale.priceForTokens.call(1000000);
        assert.strictEqual(priceForTokens.toNumber(), 100000000, "price for 1 token is incorrect");

        priceForTokens = await cryptoequityPresale.priceForTokens.call(1000000000000);
        assert.strictEqual(priceForTokens.toNumber(), 100000000000000, "price for 1m token is incorrect");

        priceForTokens = await cryptoequityPresale.priceForTokens.call(87000000000000);
        assert.strictEqual(priceForTokens.toNumber(), 8700000000000000, "price for 87m tokens is incorrect");
    });

    it("tokensForPrice should behave correctly in fourth round (time based)", async () => {

        let tokensForPrice = await cryptoequityPresale.tokensForPrice.call(99);
        assert.strictEqual(tokensForPrice.toNumber(), 0, "tokens for 99 wei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(100);
        assert.strictEqual(tokensForPrice.toNumber(), 1, "tokens for 100 wei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(101);
        assert.strictEqual(tokensForPrice.toNumber(), 1, "tokens for 101 wei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(199);
        assert.strictEqual(tokensForPrice.toNumber(), 1, "tokens for 199 wei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(200);
        assert.strictEqual(tokensForPrice.toNumber(), 2, "tokens for 200 wei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(201);
        assert.strictEqual(tokensForPrice.toNumber(), 2, "tokens for 201 wei is incorrect");

        tokensForPrice = await cryptoequityPresale.tokensForPrice.call(8700000000000000);
        assert.strictEqual(tokensForPrice.toNumber(), 87000000000000, "tokens for 8700 twei is incorrect");
    });

    it("second account should be able to buy 1 million tokens in fourth round (time based)", async () => {

        await sendTransaction({to: cryptoequityPresale.address, from: accounts[1], value: 100000000000099, gasPrice: gasPrice});

        const secondAccountBalance = await cryptoequityToken.balanceOf(accounts[1]);
        assert.strictEqual(secondAccountBalance.toNumber(), 4000000000000, "second account had incorrect balance");
        
        const presaleBalance = await cryptoequityToken.balanceOf(cryptoequityPresale.address);
        assert.strictEqual(presaleBalance.toNumber(), 86000000000000, "presale contract had incorrect balance");
    });

    it("controller address should not be able to withdraw funds before the fourth round is complete", async () => {

        let didThrow = false;
        try {
            await cryptoequityPresale.finalizePresale({from: accounts[0], gasPrice: gasPrice, gas: gas});
        } catch (e) {
            didThrow = (e.message == "VM Exception while processing transaction: invalid opcode")
        }

        assert.isTrue(didThrow, "failed to throw when controller tried to finalize presale");
    });


    /*******************/
    /* After End Tests */
    /*******************/


    it("wait until the end of the presale", async () => {

        console.log('Waiting for end of presale...');
        const endTimestamp = await cryptoequityPresale.endTimestamp.call()
        while (Math.floor(Date.now()/1000) <= endTimestamp.valueOf()) {};
        console.log('Presale ended.');

        await sendTransaction({from: accounts[0], to: accounts[1], value: 1, gasPrice: gasPrice})

        const isEnded = await cryptoequityPresale.isEnded.call()
        assert.isTrue(isEnded, "presale has not yet ended")
    });

    it("second account should not be able to purchase tokens after presale ends", async () => {

        let didThrow = false;
        try {
            await sendTransaction({to: cryptoequityPresale.address, from: accounts[1], value: 1000000000000, gasPrice: gasPrice, gas: gas});
        } catch (e) {
            didThrow = (e.message == "VM Exception while processing transaction: invalid opcode")
        }

        assert.isTrue(didThrow, "failed to throw when buying tokens after presale end");
    });

    it("controller address should be able to withdraw ether and tokens after presale ends", async () => {

        const controllerInitialEtherBalance = await getBalance(accounts[0]);
        await cryptoequityPresale.finalizePresale({from: accounts[0], gasPrice: gasPrice});
        const controllerFinalEtherBalance = await getBalance(accounts[0]);

        const controllerEtherBalanceDiff = controllerFinalEtherBalance.minus(controllerInitialEtherBalance);
        const correctBalanceDiff = 315000000000000 - 49207 * gasPrice
        assert.strictEqual(controllerEtherBalanceDiff.toNumber(), correctBalanceDiff, "controller had wrong balance");

        const controllerTokenBalance = await cryptoequityToken.balanceOf(accounts[0]);
        assert.strictEqual(controllerTokenBalance.toNumber(), 86000000000000, "controller had wrong token balance");

        const presaleContractEtherBalance = await getBalance(cryptoequityPresale.address);
        assert.strictEqual(presaleContractEtherBalance.toNumber(), 0, "presale contract had wrong ether balance");

        const presaleContractTokenBalance = await cryptoequityToken.balanceOf(cryptoequityPresale.address);
        assert.strictEqual(presaleContractTokenBalance.toNumber(), 0, "presale contract had wrong token balance");
    });
});
