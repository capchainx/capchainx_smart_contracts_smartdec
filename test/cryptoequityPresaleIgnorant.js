var sleep = require('sleep');

var CryptoequityToken = artifacts.require("./CryptoequityToken.sol");
var CryptoequityPresale = artifacts.require("./CryptoequityPresale.sol");

contract('CryptoequityPresaleIgnorant', function(accounts) {

    /*************/
    /* Utilities */
    /*************/

    // before each test, initialize the cryptoequityToken and cryptoequityPresale variables from the deployed contracts
    beforeEach(async () => {
        cryptoequityToken = await CryptoequityToken.deployed()
        cryptoequityPresale = await CryptoequityPresale.deployed()
    })

    const promisify = (inner) =>
      new Promise((resolve, reject) =>
        inner((err, res) => {
          if (err) { reject(err) }
          resolve(res);
        })
      );
    
    const sendTransaction = (data) =>
      promisify(cb => web3.eth.sendTransaction(data, cb));
    
    const getBalance = (address) =>
      promisify(cb => web3.eth.getBalance(address, cb));

    //const gasPrice = 10000000000;
    const gasPrice = 1;
    const gas = 100000;

    /***********************/
    /* Ignorant User Tests */
    /***********************/

    it("send initial tokens and wait until first round starts", async () => {

        console.log('Waiting for first round...');
        const firstRoundStart = await cryptoequityPresale.roundTimestamps.call(0)
        while (Math.floor(Date.now()/1000) <= firstRoundStart.valueOf()) {};
        console.log('First round started.');

        cryptoequityToken.transfer(cryptoequityPresale.address, 500000000000, {from: accounts[0]})

        const isStarted = await cryptoequityPresale.isStarted.call()
        assert.isTrue(isStarted, "first round has not yet started")

        const presaleBalance = await cryptoequityToken.balanceOf.call(cryptoequityPresale.address)
        assert.strictEqual(presaleBalance.toNumber(), 500000000000, "presale contract has incorrect balance")
    })

    it("second account should not be able to buy more tokens than are available", async () => {

        const priceForTokens = await cryptoequityPresale.priceForTokens.call(500000000000);
        const transactionPrice = priceForTokens.times(2).toNumber();

        await sendTransaction({to: cryptoequityPresale.address, from: accounts[1], value: transactionPrice, gas: 100000});

        const secondAccountBalance = await cryptoequityToken.balanceOf(accounts[1]);
        assert.strictEqual(secondAccountBalance.toNumber(), 500000000000, "second account has incorrect balance");
    })

    it("second account should not be able to purchase tokens after supply has run out", async () => {
        
        let didThrow = false;
        try {
            await sendTransaction({to: cryptoequityPresale.address, from: accounts[1], value: 1000000000, gas: 100000});
        } catch (e) {
            didThrow = (e.message == "VM Exception while processing transaction: invalid opcode")
        }

        assert.isTrue(didThrow, "failed to throw when buying tokens after supply has run out");
    })

    it("second account should not be able to purchase tokens after time has run out", async () => {

        // first, wait until the presale ends
        console.log('Waiting for presale to end...');
        const presaleEnd = await cryptoequityPresale.endTimestamp.call();
        while (Math.floor(Date.now()/1000) <= presaleEnd.toNumber() + 1) {};
        console.log('Presale ended.');

        // then, send some more tokens to the presale contract from the first account
        await cryptoequityToken.transfer(cryptoequityPresale.address, 500000000000, {from: accounts[0]})

        const presaleBalance = await cryptoequityToken.balanceOf.call(cryptoequityPresale.address)
        assert.strictEqual(presaleBalance.toNumber(), 500000000000, "presale contract has incorrect balance")

        const isEnded = await cryptoequityPresale.isEnded.call()
        assert.isTrue(isEnded, "presale has not yet ended")
        
        // finally, try to buy more tokens
        let didThrow = false;
        try {
            await sendTransaction({to: cryptoequityPresale.address, from: accounts[1], value: 1000000000, gas: 100000});
        } catch (e) {
            didThrow = (e.message == "VM Exception while processing transaction: invalid opcode")
        }

        assert.isTrue(didThrow, "failed to throw when buying tokens after time has run out");
    })
});
