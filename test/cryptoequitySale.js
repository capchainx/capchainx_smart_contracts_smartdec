var sleep = require('sleep');

var CryptoequityToken = artifacts.require("./CryptoequityToken.sol");
var CryptoequitySale = artifacts.require("./CryptoequitySale.sol");

contract('CryptoequitySale', function(accounts) {

    /*************/
    /* Utilities */
    /*************/

    // before each test, initialize the cryptoequityToken and cryptoequitySale variables from the deployed contracts
    beforeEach(async () => {
        cryptoequityToken = await CryptoequityToken.deployed()
        cryptoequitySale = await CryptoequitySale.deployed()
    })

    const promisify = (inner) =>
      new Promise((resolve, reject) =>
        inner((err, res) => {
          if (err) { reject(err) }
          resolve(res);
        })
      );
    
    const sendTransaction = (data) =>
      promisify(cb => web3.eth.sendTransaction(data, cb));
    
    const getBalance = (address) =>
      promisify(cb => web3.eth.getBalance(address, cb));

    //const gasPrice = 10000000000;
    const gasPrice = 1;
    const gas = 150000;


    /*****************/
    /* Initial Tests */
    /*****************/

    it("sale contract should be initialized with correct parameters", async () => {

        const controllerAddress = await cryptoequitySale.saleController.call();
        assert.strictEqual(controllerAddress, accounts[0], "Controller address was incorrect");

        const tokenAddress = await cryptoequitySale.cryptoequityToken.call();
        assert.strictEqual(tokenAddress, cryptoequityToken.address, "Token address was incorrect");

        const basePrice = await cryptoequitySale.basePrice.call();
        assert.equal(basePrice, 100, "Base price was incorrect");
    });

    it("send 10 million cryptoequity tokens from controller address to smart contract", async () => {

        await cryptoequityToken.transfer(cryptoequitySale.address, 10000000000000, {from: accounts[0], gasPrice: gasPrice});

        const saleBalance = await cryptoequityToken.balanceOf.call(cryptoequitySale.address);
        assert.strictEqual(saleBalance.toNumber(), 10000000000000, "sale contract balance was incorrect");

        const controllerBalance = await cryptoequityToken.balanceOf.call(accounts[0]);
        assert.strictEqual(controllerBalance.toNumber(), 80000000000000, "controller address balance was incorrect");
    });

    it("second account should not be able to purchase tokens before sale begins", async () => {

        let didThrow = false;
        try {
            await sendTransaction({to: cryptoequitySale.address, from: accounts[1], value: 1000000000000, gasPrice: gasPrice, gas: gas});
        } catch (e) {
            didThrow = (e.message == "VM Exception while processing transaction: invalid opcode")
        }

        assert.isTrue(didThrow, "failed to throw when buying tokens after sale ended");
    });


    /*********************/
    /* First Round Tests */
    /*********************/

    it("wait until the sale starts", async () => {

        console.log('Waiting for sale to start...');
        const startTimestamp = await cryptoequitySale.startTimestamp.call()
        while (Math.floor(Date.now()/1000) <= startTimestamp.valueOf()) {};
        console.log('Sale started.');

        await sendTransaction({from: accounts[0], to: accounts[1], value: 1, gasPrice: gasPrice})

        const isStarted = await cryptoequitySale.isStarted.call()
        assert.isTrue(isStarted, "sale has not yet started")
    });

    it("priceForTokens should behave correctly in first round (allocation based)", async () => {
        
        let priceForTokens = await cryptoequitySale.priceForTokens.call(1000000);
        assert.strictEqual(priceForTokens.toNumber(), 100000000, "price for 1 token is incorrect");

        priceForTokens = await cryptoequitySale.priceForTokens.call(1000000000);
        assert.strictEqual(priceForTokens.toNumber(), 100000000000, "price for 1k tokens is incorrect");

        priceForTokens = await cryptoequitySale.priceForTokens.call(1000000000000);
        assert.strictEqual(priceForTokens.toNumber(), 100000000000000, "price for 1m tokens is incorrect");

        priceForTokens = await cryptoequitySale.priceForTokens.call(1000000000001);
        assert.strictEqual(priceForTokens.toNumber(), 100000000000110, "price for 1m and 1 tokens is incorrect");

        priceForTokens = await cryptoequitySale.priceForTokens.call(2000000000000);
        assert.strictEqual(priceForTokens.toNumber(), 210000000000000, "price for 2m tokens is incorrect");

        priceForTokens = await cryptoequitySale.priceForTokens.call(2000000000001);
        assert.strictEqual(priceForTokens.toNumber(), 210000000000120, "price for 2m and 1 tokens is incorrect");

        priceForTokens = await cryptoequitySale.priceForTokens.call(3000000000000);
        assert.strictEqual(priceForTokens.toNumber(), 330000000000000, "price for 3m tokens is incorrect");

        priceForTokens = await cryptoequitySale.priceForTokens.call(3000000000001);
        assert.strictEqual(priceForTokens.toNumber(), 330000000000130, "price for 3m and 1 tokens is incorrect");

        // skip ahead to 10m tokens

        priceForTokens = await cryptoequitySale.priceForTokens.call(10000000000000);
        assert.strictEqual(priceForTokens.toNumber(), 1450000000000000, "price for 10m tokens is incorrect");
    });

    it("tokensForPrice should behave correctly in first round (time based)", async () => {

        let tokensForPrice = await cryptoequitySale.tokensForPrice.call(99);
        assert.strictEqual(tokensForPrice.toNumber(), 0, "tokens for 99 wei is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(100);
        assert.strictEqual(tokensForPrice.toNumber(), 1, "tokens for 100 wei is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(101);
        assert.strictEqual(tokensForPrice.toNumber(), 1, "tokens for 51 wei is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(199);
        assert.strictEqual(tokensForPrice.toNumber(), 1, "tokens for 199 wei is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(200);
        assert.strictEqual(tokensForPrice.toNumber(), 2, "tokens for 200 wei is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(201);
        assert.strictEqual(tokensForPrice.toNumber(), 2, "tokens for 201 wei is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(1000000000);
        assert.strictEqual(tokensForPrice.toNumber(), 10000000, "tokens for 1 gwei is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(100000000000000);
        assert.strictEqual(tokensForPrice.toNumber(), 1000000000000, "tokens for 100 twei is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(100000000000109);
        assert.strictEqual(tokensForPrice.toNumber(), 1000000000000, "tokens for 100 twei + 109 is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(100000000000110);
        assert.strictEqual(tokensForPrice.toNumber(), 1000000000001, "tokens for 100 twei + 110 is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(100000000000219);
        assert.strictEqual(tokensForPrice.toNumber(), 1000000000001, "tokens for 100 twei + 219 is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(100000000000220);
        assert.strictEqual(tokensForPrice.toNumber(), 1000000000002, "tokens for 100 twei + 220 is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(210000000000000);
        assert.strictEqual(tokensForPrice.toNumber(), 2000000000000, "tokens for 210 twei is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(210000000000119);
        assert.strictEqual(tokensForPrice.toNumber(), 2000000000000, "tokens for 210 twei + 119 is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(210000000000120);
        assert.strictEqual(tokensForPrice.toNumber(), 2000000000001, "tokens for 210 twei + 120 is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(210000000000239);
        assert.strictEqual(tokensForPrice.toNumber(), 2000000000001, "tokens for 210 twei + 239 is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(210000000000240);
        assert.strictEqual(tokensForPrice.toNumber(), 2000000000002, "tokens for 210 twei + 240 is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(330000000000000);
        assert.strictEqual(tokensForPrice.toNumber(), 3000000000000, "tokens for 330 twei is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(330000000000129);
        assert.strictEqual(tokensForPrice.toNumber(), 3000000000000, "tokens for 330 twei + 129 is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(330000000000130);
        assert.strictEqual(tokensForPrice.toNumber(), 3000000000001, "tokens for 330 twei + 130 is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(330000000000259);
        assert.strictEqual(tokensForPrice.toNumber(), 3000000000001, "tokens for 330 twei + 259 is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(330000000000260);
        assert.strictEqual(tokensForPrice.toNumber(), 3000000000002, "tokens for 330 twei + 260 is incorrect");

        // skip ahead to 10m tokens

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(1450000000000000);
        assert.strictEqual(tokensForPrice.toNumber(), 10000000000000, "tokens for 1450 twei is incorrect");
    });

    it("second account should be able to buy 1 million tokens in first round (allocation based)", async () => {

        await sendTransaction({to: cryptoequitySale.address, from: accounts[1], value: 100000000000000, gasPrice: gasPrice, gas: gas});

        const secondAccountBalance = await cryptoequityToken.balanceOf(accounts[1]);
        assert.strictEqual(secondAccountBalance.toNumber(), 1000000000000, "second account had incorrect balance");
        
        const saleBalance = await cryptoequityToken.balanceOf(cryptoequitySale.address);
        assert.strictEqual(saleBalance.toNumber(), 9000000000000, "sale contract had incorrect balance");
    });


    /**********************/
    /* Second Round Tests */
    /**********************/

    it("priceForTokens should behave correctly in second round (allocation based)", async () => {
        
        let priceForTokens = await cryptoequitySale.priceForTokens.call(1000000);
        assert.strictEqual(priceForTokens.toNumber(), 110000000, "price for 1 token is incorrect");

        priceForTokens = await cryptoequitySale.priceForTokens.call(1000000000);
        assert.strictEqual(priceForTokens.toNumber(), 110000000000, "price for 1k tokens is incorrect");

        priceForTokens = await cryptoequitySale.priceForTokens.call(1000000000000);
        assert.strictEqual(priceForTokens.toNumber(), 110000000000000, "price for 1m tokens is incorrect");

        priceForTokens = await cryptoequitySale.priceForTokens.call(1000000000001);
        assert.strictEqual(priceForTokens.toNumber(), 110000000000120, "price for 1m and 1 tokens is incorrect");

        priceForTokens = await cryptoequitySale.priceForTokens.call(2000000000000);
        assert.strictEqual(priceForTokens.toNumber(), 230000000000000, "price for 2m tokens is incorrect");

        priceForTokens = await cryptoequitySale.priceForTokens.call(2000000000001);
        assert.strictEqual(priceForTokens.toNumber(), 230000000000130, "price for 2m and 1 tokens is incorrect");

        priceForTokens = await cryptoequitySale.priceForTokens.call(3000000000000);
        assert.strictEqual(priceForTokens.toNumber(), 360000000000000, "price for 3m tokens is incorrect");

        priceForTokens = await cryptoequitySale.priceForTokens.call(3000000000001);
        assert.strictEqual(priceForTokens.toNumber(), 360000000000140, "price for 3m and 1 tokens is incorrect");

        // skip ahead to 9m tokens

        priceForTokens = await cryptoequitySale.priceForTokens.call(9000000000000);
        assert.strictEqual(priceForTokens.toNumber(), 1350000000000000, "price for 9m tokens is incorrect");
    });

    it("tokensForPrice should behave correctly in second round (allocation based)", async () => {

        let tokensForPrice = await cryptoequitySale.tokensForPrice.call(109);
        assert.strictEqual(tokensForPrice.toNumber(), 0, "tokens for 109 wei is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(110);
        assert.strictEqual(tokensForPrice.toNumber(), 1, "tokens for 110 wei is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(111);
        assert.strictEqual(tokensForPrice.toNumber(), 1, "tokens for 111 wei is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(219);
        assert.strictEqual(tokensForPrice.toNumber(), 1, "tokens for 219 wei is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(220);
        assert.strictEqual(tokensForPrice.toNumber(), 2, "tokens for 220 wei is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(221);
        assert.strictEqual(tokensForPrice.toNumber(), 2, "tokens for 221 wei is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(110000000000);
        assert.strictEqual(tokensForPrice.toNumber(), 1000000000, "tokens for 110 gwei is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(110000000000000);
        assert.strictEqual(tokensForPrice.toNumber(), 1000000000000, "tokens for 110 twei is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(110000000000119);
        assert.strictEqual(tokensForPrice.toNumber(), 1000000000000, "tokens for 110 twei + 119 is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(110000000000120);
        assert.strictEqual(tokensForPrice.toNumber(), 1000000000001, "tokens for 110 twei + 120 is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(110000000000239);
        assert.strictEqual(tokensForPrice.toNumber(), 1000000000001, "tokens for 110 twei + 239 is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(110000000000240);
        assert.strictEqual(tokensForPrice.toNumber(), 1000000000002, "tokens for 110 twei + 240 is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(230000000000000);
        assert.strictEqual(tokensForPrice.toNumber(), 2000000000000, "tokens for 230 twei is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(230000000000129);
        assert.strictEqual(tokensForPrice.toNumber(), 2000000000000, "tokens for 230 twei + 129 is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(230000000000130);
        assert.strictEqual(tokensForPrice.toNumber(), 2000000000001, "tokens for 230 twei + 130 is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(230000000000259);
        assert.strictEqual(tokensForPrice.toNumber(), 2000000000001, "tokens for 230 twei + 259 is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(230000000000260);
        assert.strictEqual(tokensForPrice.toNumber(), 2000000000002, "tokens for 230 twei + 260 is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(360000000000000);
        assert.strictEqual(tokensForPrice.toNumber(), 3000000000000, "tokens for 360 twei is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(360000000000139);
        assert.strictEqual(tokensForPrice.toNumber(), 3000000000000, "tokens for 360 twei + 139 is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(360000000000140);
        assert.strictEqual(tokensForPrice.toNumber(), 3000000000001, "tokens for 360 twei + 140 is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(360000000000279);
        assert.strictEqual(tokensForPrice.toNumber(), 3000000000001, "tokens for 360 twei + 279 is incorrect");

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(360000000000280);
        assert.strictEqual(tokensForPrice.toNumber(), 3000000000002, "tokens for 360 twei + 280 is incorrect");

        // skip ahead to 9m tokens

        tokensForPrice = await cryptoequitySale.tokensForPrice.call(1350000000000000);
        assert.strictEqual(tokensForPrice.toNumber(), 9000000000000, "tokens for 1350 twei is incorrect");
    });

    it("second account should be able to buy 1 million tokens in second round (allocation based)", async () => {

        await sendTransaction({to: cryptoequitySale.address, from: accounts[1], value: 110000000000000, gasPrice: gasPrice});

        const secondAccountBalance = await cryptoequityToken.balanceOf(accounts[1]);
        assert.strictEqual(secondAccountBalance.toNumber(), 2000000000000, "second account had incorrect balance");
        
        const saleBalance = await cryptoequityToken.balanceOf(cryptoequitySale.address);
        assert.strictEqual(saleBalance.toNumber(), 8000000000000, "sale contract had incorrect balance");
    });

    /**************************/
    /* Early Withdrawal Tests */
    /**************************/

    it("withdrawEther successfully withdraws ether", async () => {
	let saleController = await cryptoequitySale.saleController.call();

        // check that the sale has not yet ended
        const isEnded = await cryptoequitySale.isEnded.call()
        assert.isFalse(isEnded, "sale has ended")

        // get the initial ether balances of the controller and the sale
        const controllerInitialEtherBalance = await getBalance(saleController);
        const saleInitialEtherBalance = await getBalance(cryptoequitySale.address);

        // call withdrawEther
        await cryptoequitySale.withdrawEther({from: saleController, gasPrice: gasPrice});

        // get the final ether balances of the controller and the sale
        const controllerFinalEtherBalance = await getBalance(saleController);
        const saleFinalEtherBalance = await getBalance(cryptoequitySale.address);

        // check the controller ether balance
        const controllerEtherBalanceDiff = controllerFinalEtherBalance.minus(controllerInitialEtherBalance);
        assert.strictEqual(controllerEtherBalanceDiff.toNumber(), 210000000000000 - 30207, "controller had wrong ether balance");

        // check the sale ether balance
        const saleEtherBalanceDiff = saleFinalEtherBalance.minus(saleInitialEtherBalance);
        assert.strictEqual(saleEtherBalanceDiff.toNumber(), -210000000000000, "sale had wrong ether balance");
    });

    it("withdrawTokens successfully withdraws tokens", async () => {
	let saleController = await cryptoequitySale.saleController.call();

        // check that the sale has not yet ended
        const isEnded = await cryptoequitySale.isEnded.call()
        assert.isFalse(isEnded, "sale has ended")

        // get the initial token balances of the controller and the sale
        const controllerInitialTokenBalance = await cryptoequityToken.balanceOf(saleController);
        const saleInitialTokenBalance = await cryptoequityToken.balanceOf(cryptoequitySale.address);

        // call withdrawTokens for 500000000000 tokens (leaving the contract with 7500000000000)
        await cryptoequitySale.withdrawTokens(500000000000, {from: saleController, gasPrice: gasPrice});

        // get the final token balances of the controller and the sale
        const controllerFinalTokenBalance = await cryptoequityToken.balanceOf(saleController);
        const saleFinalTokenBalance = await cryptoequityToken.balanceOf(cryptoequitySale.address);

        // check the controller token balance
        const controllerTokenBalanceDiff = controllerFinalTokenBalance.minus(controllerInitialTokenBalance);
        assert.strictEqual(controllerTokenBalanceDiff.toNumber(), 500000000000, "controller had wrong token balance");

        // check the sale token balance
        const saleTokenBalanceDiff = saleFinalTokenBalance.minus(saleInitialTokenBalance);
        assert.strictEqual(saleTokenBalanceDiff.toNumber(), -500000000000, "sale had wrong token balance");
    });

    it("sale continues after withdrawing ether and tokens", async () => {

        // check that the sale has not yet ended
        const isEnded = await cryptoequitySale.isEnded.call()
        assert.isFalse(isEnded, "sale has ended")

        const buyerInitialEtherBalance = await getBalance(accounts[1]);
        const buyerInitialTokenBalance = await cryptoequityToken.balanceOf(accounts[1]);

	    // buy another 500k tokens from accounts[1]
	    await sendTransaction({to: cryptoequitySale.address, from: accounts[1], value: 60000000000000, gasPrice: gasPrice});

        const buyerFinalEtherBalance = await getBalance(accounts[1]);
        const buyerFinalTokenBalance = await cryptoequityToken.balanceOf(accounts[1]);

        const buyerEtherBalanceDiff = buyerFinalEtherBalance.minus(buyerInitialEtherBalance);
        assert.strictEqual(buyerEtherBalanceDiff.toNumber(), -60000000000000 - 84847, "buyer had wrong ether balance");

        const buyerTokenBalanceDiff = buyerFinalTokenBalance.minus(buyerInitialTokenBalance);
        assert.strictEqual(buyerTokenBalanceDiff.toNumber(), 500000000000, "buyer had wrong token balance");

    });


    /*******************/
    /* After End Tests */
    /*******************/

    it("wait until the end of the sale", async () => {

        console.log('Waiting for end of sale...');
        const endTimestamp = await cryptoequitySale.endTimestamp.call()
        while (Math.floor(Date.now()/1000) <= endTimestamp.valueOf()) {};
        console.log('Sale ended.');

        await sendTransaction({from: accounts[0], to: accounts[1], value: 1, gasPrice: gasPrice})

        const isEnded = await cryptoequitySale.isEnded.call()
        assert.isTrue(isEnded, "sale has not yet ended")
    });

    it("second account should not be able to purchase tokens after sale ends", async () => {

        let didThrow = false;
        try {
            await sendTransaction({to: cryptoequitySale.address, from: accounts[1], value: 1000000000000, gasPrice: gasPrice, gas: gas});
        } catch (e) {
            didThrow = (e.message == "VM Exception while processing transaction: invalid opcode")
        }

        assert.isTrue(didThrow, "failed to throw when buying tokens after sale end");
    });

    it("controller address should be able to withdraw ether and tokens after sale ends", async () => {

        const controllerInitialEtherBalance = await getBalance(accounts[0]);
        await cryptoequitySale.finalizeSale({from: accounts[0], gasPrice: gasPrice});
        const controllerFinalEtherBalance = await getBalance(accounts[0]);

        const controllerEtherBalanceDiff = controllerFinalEtherBalance.minus(controllerInitialEtherBalance);
        const correctBalanceDiff = 60000000000000 - 34141 * gasPrice
        assert.strictEqual(controllerEtherBalanceDiff.toNumber(), correctBalanceDiff, "controller had wrong balance");

        const controllerTokenBalance = await cryptoequityToken.balanceOf(accounts[0]);
        assert.strictEqual(controllerTokenBalance.toNumber(), 87500000000000, "controller had wrong token balance");

        const saleContractEtherBalance = await getBalance(cryptoequitySale.address);
        assert.strictEqual(saleContractEtherBalance.toNumber(), 0, "sale contract had wrong ether balance");

        const saleContractTokenBalance = await cryptoequityToken.balanceOf(cryptoequitySale.address);
        assert.strictEqual(saleContractTokenBalance.toNumber(), 0, "sale contract had wrong token balance");
    });
});
