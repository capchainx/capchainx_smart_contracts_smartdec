var Web3 = require('web3');
var BN = require('bignumber.js');
var sleep = require('sleep');

var CryptoequityPresale = artifacts.require("./CryptoequityPresale.sol");
var CryptoequityToken = artifacts.require("./CryptoequityToken.sol");



contract('CryptoequityPresaleEmergency', function(accounts) {

    /*************/
    /* Utilities */
    /*************/

    // before each test, initialize the cryptoequityToken and cryptoequityPresale variables from the deployed contracts
    beforeEach(async () => {
	cryptoequityPresale = await CryptoequityPresale.deployed();
        cryptoequityToken = await CryptoequityToken.deployed();
    })

    const promisify = (inner) =>
      new Promise((resolve, reject) =>
        inner((err, res) => {
          if (err) { reject(err) }
          resolve(res);
        })
      );
    
    const sendTransaction = (data) =>
      promisify(cb => web3.eth.sendTransaction(data, cb));


    const getBalance = (address) =>
      promisify(cb => web3.eth.getBalance(address, cb));

    //const gasPrice = 10000000000;
    const gasPrice = 1;
    const gas = 100000;


    /************************/
    /* Emergency User Tests */
    /************************/
    it("send initial tokens and wait until first round starts", async () => {
        console.log('Waiting for first round...');
        const firstRoundStart = await cryptoequityPresale.roundTimestamps.call(0)
        while (Math.floor(Date.now()/1000) <= firstRoundStart.valueOf()) {};
        console.log('First round started.');

        cryptoequityToken.transfer(cryptoequityPresale.address, 90000000000000, {from: accounts[0], gasPrice: gasPrice});

        const isStarted = await cryptoequityPresale.isStarted.call();

	    // Buy 1ETH of tokens so that presale contract has some ether
	    await sendTransaction({to: cryptoequityPresale.address, from: accounts[1], value: 1000000000000, gasPrice: gasPrice});
    })

    it("Declare emergency successfully disables buying function", async () => {
	let presaleController = await cryptoequityPresale.presaleController.call();
        await cryptoequityPresale.declareEmergency({from: presaleController});

        let didThrow = false;
        try {
            await sendTransaction({to: cryptoequityPresale.address, from: accounts[1], value: 1000000000000, gasPrice: gasPrice, gas: gas});
        } catch (e) {
            //didThrow = (e.message == "VM Exception while processing transaction: invalid opcode")
            didThrow = true; //(e.message == "VM Exception while processing transaction: invalid opcode")
        }

        assert.isTrue(didThrow, "failed to throw after declaring an emergency");
    });

    it("Funds can be withdrawn after emergency is declared", async () => {
	// First assert that we haven't passed the endTimestamp
	let endTimestamp = await cryptoequityPresale.endTimestamp.call();
	var currentTimestamp = new Date() / 1000;
	assert.isTrue(endTimestamp > currentTimestamp, "the presale ending time has already passed");

        const controllerInitialEtherBalance = await getBalance(accounts[0]);
        await cryptoequityPresale.finalizePresale({from: accounts[0], gasPrice: gasPrice});
        const controllerFinalEtherBalance = await getBalance(accounts[0]);

        const controllerEtherBalanceDiff = controllerFinalEtherBalance.minus(controllerInitialEtherBalance);
        assert.strictEqual(controllerEtherBalanceDiff.toNumber(), 1000000000000 - 48994, "controller had wrong balance");

        const controllerTokenBalance = await cryptoequityToken.balanceOf(accounts[0]);
        assert.strictEqual(controllerTokenBalance.toNumber(), 89980000000000, "controller had wrong token balance");

        const investorTokenBalance = await cryptoequityToken.balanceOf(accounts[1]);
        assert.strictEqual(investorTokenBalance.toNumber(), 20000000000, "investor had wrong token balance");

        const presaleContractEtherBalance = await getBalance(cryptoequityPresale.address);
        assert.strictEqual(presaleContractEtherBalance.toNumber(), 0, "presale contract had wrong ether balance");

        const presaleContractTokenBalance = await cryptoequityToken.balanceOf(cryptoequityPresale.address);
        assert.strictEqual(presaleContractTokenBalance.toNumber(), 0, "presale contract had wrong token balance");
    });
});
